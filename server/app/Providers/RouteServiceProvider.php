<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $memberNamespace = 'App\Http\Controllers\Member';
    protected $instructorNamespace = 'App\Http\Controllers\Instructor';
    protected $admin100Namespace = 'App\Http\Controllers\Admin100';
    protected $authNamespace = 'App\Http\Controllers\AuthApi';
    protected $superadminNamespace = 'App\Http\Controllers\Superadmin';


    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //


        // $this->registerPolicies();

        // Passport::routes();

      
        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        $this->mapMemberApiRoutes();
        $this->mapInstructorApiRoutes();
        $this->mapInstructorAuthApiRoutes();
        $this->mapAdmin100AuthApiRoutes();
        $this->mapSuperadminAuthApiRoutes();
        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->namespace)
             ->group(base_path('routes/api.php'));
    }


    protected function mapMemberApiRoutes()
    {
        Route::prefix('member')
            ->middleware(['web','auth','member'])
            ->namespace($this->memberNamespace)
            ->group(base_path('routes/member.php'));
    }

    protected function mapInstructorApiRoutes()
    {
        Route::prefix('instructor')
            ->middleware(['web','auth','instructor'])
            ->namespace($this->instructorNamespace)
            ->group(base_path('routes/instructor.php'));
    }

    protected function mapInstructorAuthApiRoutes(){
        Route::prefix('')
            ->middleware(['web','auth'])
            ->namespace($this->authNamespace)
            ->group(base_path('routes/auth.php'));
    }

    protected function mapAdmin100AuthApiRoutes(){
        Route::prefix('admin100')
            ->middleware(['api'])
            ->namespace($this->admin100Namespace)
            ->group(base_path('routes/admin100.php'));
    }

    protected function mapSuperadminAuthApiRoutes(){
        Route::prefix('superadmin')
            ->middleware(['api'])
            ->namespace($this->superadminNamespace)
            ->group(base_path('routes/superadmin.php'));
    }
}
