<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Instructor
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(!Auth::user()->is_merchant){
            return response("You are not a merchant",401);
        }
        return $response;
    }
}