<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Member
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if(Auth::user()->is_merchant){
            return response("You are not a normal user",401);
        }
        return $response;
    }
}