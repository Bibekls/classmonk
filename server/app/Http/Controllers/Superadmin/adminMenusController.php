<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Model\AdminMenu;
use App\Model\Admin;
use App\Model\AdminAccessMenu;
class adminMenusController extends Controller{
  public function getMenus(AdminMenu $menuslist){
     $menus=$menuslist->with('submenus')->get();
     return response()->json(['menus'=>$menus]);
  }
  public function AdminUser(Request $request, Admin $admin){
               if($request['status'] != false){
                                  $firstname=$request['firstname'];
                                  $middlename=$request['middlename'];
                                  $lastname=$request['lastname'];
                                  $address=$request['address'];
                                  $country=$request['country'];
                                  $phoneNumber=$request['PhoneNumber'];
                                  $email=$request['email'];
                                  $password=$request['password'];
                                  $UserId=$request['userid'];
               $user=Admin::where('id', $UserId)
                               ->update([
                                    'firstname'=>$firstname,
                                    'middlename'=>$middlename,
                                    'lastname'=>$lastname,
                                    'address'=>$address,
                                    'country'=>$country,
                                    'phoneNumber'=>$phoneNumber,
                                    'email'=>$email,
                                    'password'=>bcrypt($password)
                               ]);
                           $deleteAccesMenus=AdminAccessMenu::where('admin_id', $UserId)->delete();
                              $menus=$request['menu'];
                                foreach($menus as $menu){
                                    $acessmenus=AdminAccessMenu::create([
                                        'admin_menus_id'=>$menu['id'],
                                        'admin_id'=>$request['userid'],
                                        'permission'=>$menu['permission']
                                    ]);   
                                }
                            $admin_data=$admin->with('menus')->where('id', $UserId)->get();
                            return response()->json(['user'=>$user,'data'=>$admin_data]);  
                       }else{
                                $firstname=$request['firstname'];
                                $middlename=$request['middlename'];
                                $lastname=$request['lastname'];
                                $address=$request['address'];
                                $country=$request['country'];
                                $phoneNumber=$request['PhoneNumber'];
                                $email=$request['email'];
                                $password=$request['password'];
                                $user=Admin::create([
                                  'firstname'=>$firstname,
                                  'middlename'=>$middlename,
                                  'lastname'=>$lastname,
                                  'address'=>$address,
                                  'country'=>$country,
                                  'phoneNumber'=>$phoneNumber,
                                  'email'=>$email,
                                  'password'=>bcrypt($password),
                                ]);
                                $userId=$user->id;
                                $menus=$request['menu'];
                                if(count($user) > 0){
                                    foreach($menus as $menu){
                                        $acessmenus=AdminAccessMenu::create([
                                          'admin_menus_id'=>$menu['id'],
                                          'admin_id'=>$userId,
                                          'permission'=>$menu['permission']
                                        ]);   
                                    }
                                }
                                return response()->json(['user'=>$user]);  
                        }
         }
        // public function getactiveAdmin(){
        //   $admin=Admin::where('inactive',0)->get();
        //     return response()->json(['admin'=>$admin]);   
        // }
        public function getactiveAdmin(Admin $admin){
            $admin_data=$admin->with('menus')->where('admins.inactive',0)->get();
            $admin=Admin::where('inactive',1)->count();
            return response()->json(['admin'=>$admin_data,'inactive'=>$admin]);   
        }
        public function editAdmin($id){
            $menus=AdminAccessMenu::where('admin_id',$id)->get();
            return response()->json(['menus'=>$menus]);  
        }
        public function Make_Inactive_Admin($id){
            $inactive=Admin::where('id',$id)->update(['inactive'=>1]);
            return response()->json(['inactive'=>$inactive]); 
        }
        public function get_Inactive_Admin(){
            $admin=Admin::where('inactive',1)->get();
            return response()->json(['admin'=>$admin]); 
        }
        public function Active_Admin($id){
            $active=Admin::where('id',$id)->update(['inactive'=>0]);
            return response()->json(['active'=>$active]); 
        }
  }
