<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\City;
use App\Model\Product;
class addCityController extends Controller
{
    public function postCity(Request $request){
        $city=$request->all();
        if($request['status']==true){
            $updateCity=City::where('id',$request['cityId'])
                    ->update([
                        'name'=>$request['cityName'],
                        'slug'=>$request['slug'],
                        'address'=>$request['searchControl']
                    ]);
           return response()->json(['city'=>$updateCity]);
        }else{
            $InsertCity= City::create([
                'name'=>$request['cityName'],
                'slug'=>$request['slug'],
                'address'=>$request['searchControl']
            ]);
            return response()->json(['city'=>$InsertCity]);
        }  
    }
    public function getCity(){
        $cities=City::where('is_deleted',0)->get();
        return response()->json(['cities'=>$cities]);
    }
    public function getdeletedCity(){
        $cities=City::where('is_deleted',1)->get();
        return response()->json(['cities'=>$cities]);
    }
    public function deleteCity(Request $request){
        $address=$request['address'];
        $productLists=Product::where('name',$address)->count();
       if($productLists > 0){
          return response()->json(['address'=>'Sorry! Not allowed to Delete this City','status'=>1]);
       }else{
           $cities=City::where('address',$address)->update([
               'is_deleted'=>1
           ]);
           return response()->json(['address'=>'Successfully Deleted City','status'=>2]);
       
     }
  }
  public function reuseCity($id){
    $cities=City::where('id',$id)->update([
        'is_deleted'=>0
    ]);
    return response()->json(['address'=>'Successfully Reuse City']);
  }
}