<?php

namespace App\Http\Controllers\Superadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Category;
use App\Model\Product;
use DB;
class categoryController extends Controller
{
   public function postCategory(Request $request,Category $categories){
       if($request['status']==true){
           $id=$request['edit_id'];
           if($request['parent_id'] == null){
            $updatecategory=Category::where('id',$id)->update([
                    'parent_id'=>0,
                    'name'=>$request['categoryName'],
                    'slug'=>$request['slug'],
                    'description'=>$request['discription'],
                    'sort_order'=>1,
                    'is_class'=>$request['is_class'],
                    'status'=>1 
                    ]);
                $categories = $categories->with('parentCategory')->where('id',$id)->get();
                return response()->json(['category'=>$updatecategory,'data'=>$categories]);        
           }else{
            $updatecategory=Category::where('id',$id)->update([
                    'parent_id'=>$request['parent_id'],
                    'name'=>$request['categoryName'],
                    'slug'=>$request['slug'],
                    'description'=>$request['discription'],
                    'sort_order'=>1,
                    'is_class'=>$request['is_class'],
                    'status'=>1
             ]);
             $categories = $categories->with('parentCategory')->where('id',$id)->get();
             return response()->json(['category'=>$updatecategory,'data'=>$categories]);  
          } 
       }else{
        if($request['parent_id'] == null){
            $categories=Category::create([
                'parent_id'=>0,
                'name'=>$request['categoryName'],
                'slug'=>$request['slug'],
                'description'=>$request['discription'],
                'sort_order'=>1,
                'is_class'=>$request['is_class'],
                'status'=>1,
            ]);
            return response()->json(['category'=>$categories]);
            }else{
                $categories=Category::create([
                'parent_id'=>$request['parent_id'],
                'name'=>$request['categoryName'],
                'slug'=>$request['slug'],
                'description'=>$request['discription'],
                'sort_order'=>1,
                'is_class'=>$request['is_class'],
                'status'=>1
             ]);
             return response()->json(['category'=>$categories]);
           }
       }
   }
   public function parentCategory(){
        $parentcategory=Category::where('parent_id',0)->get();
        return response()->json(['parentcategory'=>$parentcategory]);
   }
   public function getcategoryBysearch($id){
        $category =Category::where('name', 'like', '%'.$id.'%')->where('parent_id',0)->get();
        return response()->json(['parentcategory'=>$category]);
   }

   public function getpublishedCategory(Category $categories){
       $categories = $categories->with('parentCategory')->get();
       return response()->json(['categories'=>$categories]);
   }
   public function getUnPublishedCategory(Category $categories){
       $categories = $categories->with('parentCategory')->withTrashed()->whereNotNull('deleted_at')->get();
       return response()->json(['categories'=>$categories]);
   }

   public function unPublishCategory($id){
       $productLists=Product::where('category_id',$id)->count();
       if($productLists > 0){
           return response()->json(['category'=>'Not allowed to Unpublished']);
       }
       $category=Category::where('id',$id)->delete();
          return response()->json(['category'=>'Sucessfully Unpublished']);
    }
    public function PublishCategory($id){
        $category=Category::where('id',$id)->update(['deleted_at'=>null]);
        return response()->json(['category'=>'Sucessfully published']);
    }
}
