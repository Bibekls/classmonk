<?php

namespace App\Http\Controllers\Member;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Auth;

class RegisterController extends Controller
{
    public function register(Request $request){

        $user = User::create([
            'email'=> $request->input('email'),
            'password' => bcrypt($request->input('password'))
        ]);

        Auth::loginUsingId($user->id, true);
        return response()->json([
            "user"=>$user
        ]);

    }
}
