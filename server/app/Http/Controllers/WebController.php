<?php

namespace App\Http\Controllers;

use App\Model\Category;
use App\Model\City;
use App\Model\CommonTable;
use App\Model\Product;
use Illuminate\Http\Request;

use GuzzleHttp\Client;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use Illuminate\Support\Facades\Auth;

class WebController extends Controller
{

    public function memberRegister(Request $request)
    {
    }

    public function socialLogin(Request $request)
    {
        $http = new Client;

        if ($request->has('token') && $request->has('uid')) {

            if ($request->input('provider') == "facebook") {
                $user = Socialite::driver('facebook')->userFromToken($request->input('token'));
            } elseif ($request->input('provider') == "google") {
                $user = Socialite::driver('google')->userFromToken($request->input('token'));
            }

            if ($user->getEmail() == $request->input('email')) {
                $user = \App\User::where('email', $request->input('email'))->first();
                if ($user) {
                    Auth::loginUsingId($user->id, true);
                    return response()->json([
                        "user" => $user
                    ]);

                } else {
                    // register new user

                    $newUser = \App\User::create([
                        'email' => $request->input('email')
                    ]);

                    \App\Model\SocialAccount::create([
                        'user_id' => $newUser->id,
                        'social_user_id' => $request->input('uid'),
                        'signup_media' => 'facebook',
                        'avatar' => ''
                    ]);

                    Auth::loginUsingId($newUser->id, true);
                    return response()->json([
                        "user" => $newUser
                    ]);
                }

            } else {
                return response("authentication error");
            }
        }
    }

    public function connect(Request $request)
    {
        return Auth::attempt(['email' => $request->input('email'), 'password' => $password]);

        $http = new Client;

        if ($request->has('token') && $request->has('uid')) {

            if ($request->input('provider') == "facebook") {
                $user = Socialite::driver('facebook')->userFromToken($request->input('token'));
            } elseif ($request->input('provider') == "google") {
                $user = Socialite::driver('google')->userFromToken($request->input('token'));
            }

            if ($user->getEmail() == $request->input('email')) {


                //

                //vendor\laravel\passport\src\Bridge\userrepository
                //
                //  if(env('APP_KEY')!=$password)return; is modified
                //
                // login user
                $user = \App\User::where('email', $request->input('email'))->first();
                if ($user) {

                    $response = $http->post(env('APP_URL') . '/oauth/token', [
                        'form_params' => [
                            'grant_type' => 'password',
                            'client_id' => '2',
                            'client_secret' => env('APP_CLIENT'),
                            'username' => $user->email,
                            'password' => env('APP_KEY'),
                        ],
                    ]);

                } else {
                    // register new user

                    $newUser = \App\User::create([
                        'email' => $request->input('email')
                    ]);

                    \App\Model\SocialAccount::create([
                        'user_id' => $newUser->id,
                        'social_user_id' => $request->input('uid'),
                        'signup_media' => 'facebook',
                        'avatar' => ''
                    ]);

                    $response = $http->post(env('APP_URL') . '/oauth/token', [
                        'form_params' => [
                            'grant_type' => 'password',
                            'client_id' => '2',
                            'client_secret' => env('APP_CLIENT'),
                            'username' => $request->input('email'),
                            'password' => env('APP_KEY'),
                        ],
                    ]);

                }

            } else {
                return response("authentication error");
            }
        } else {
            $response = $http->post(env('APP_URL') . '/oauth/token', [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => '2',
                    'client_secret' => env('APP_CLIENT'),
                    'username' => $request->input("email"),
                    'password' => $request->input("password"),
//                'redirect_uri' => env('APP_URL'),
                    //'code' => $request->code,
                ],
            ]);
        }

        return [
            "access_token" => json_decode((string)$response->getBody(), true)['access_token'],
            "user" => json_decode(
                \App\User::with('merchant.coverImage', 'profileImage', 'productExist')
                    ->where('email', $request->input('email'))
                    ->select('email', 'phone', 'id', 'is_merchant', 'profile_pic')
                    ->first())
        ];
    }

    public function validateEmail(Request $request)
    {
        $user = User::where('email', $request->input('email'))->select('email as email')->first();
        if ($user) return $user;
        return response()->json(["status" => "notfound"]);
    }

    public function checkAuthentication(Request $request)
    {
        return response()->json([
            "user" => \App\User::with('merchant.coverImage', 'profileImage', 'productExist')
                ->where('id', Auth::id())
                ->select('email', 'phone', 'id', 'is_merchant', 'profile_pic')
                ->first()
            , "csrf_token" => csrf_token()
        ]);
    }

    public function getCityList()
    {
        return City::all();
    }

    public function getCategoryList()
    {
        return Category::all();
    }

    public function getAmenitiesList()
    {
        $amenity = CommonTable::where("key", "amenities")->first();
        return $amenity->childs;
    }

    public function getProduct($city, $category, $slug)
    {

        $product = Product::with('images', 'coverImage', 'category', 'owner.profileImage', 'classPackages.classShifts', 'classShifts')
            ->where('slug', $slug)->first();

        $category = Category::where('slug', $category)->first();
        if (!$category) {
            return response()->json("invalid category");
        }
        if (!($category->id == $product->category_id)) {
            return response()->json("Category not match");
        }

        $city = City::where('slug', $city)->first();

        $targetCity = $product->cities->pluck('city_id');
        $targetCity = json_decode(json_encode($targetCity));

        if (!$city) {
            return response()->json("invalid city");
        }
        if (!in_array($city->id, $targetCity)) {
            return response()->json("City doesnot exist");
        }
        $product->amenities = json_decode($product->amenities);
        return $product;
    }

    public function cityProduct(Request $request)
    {
        $category = Category::where('slug',$request->input('filter')['category'])->first();

        $keyword = $request->input('filter')['keyword'];

        $city = City::where('slug', $request->input('cityName'))->first();
        $productList = Product::with('images', 'coverImage','category')
            ->join('city_products', 'products.id', '=','city_products.product_id')
//            ->join('categories', 'products.category_id', '=','categories.id')
            ->where('city_products.city_id',$city->id)
            ->where(function($sql) use($category,$keyword) {
                if($category)
                    $sql->where('products.category_id',$category->id);
                if(trim($keyword)) {
                    $sql->where('products.slug', 'like', '%' . $keyword . '%');
//                    ->orWhere('categories.slug', 'like', '%' . $keyword . '%');
                }
            })
            ->paginate(18);

        return $productList;
    }

}
