<?php

namespace App\Http\Controllers\Admin100;
use App\Model\MerchantDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class instructorController extends Controller
{
    public function getIntructor(){
        $instructor=MerchantDetail::join('users','users.id','=','merchant.user_id')->where('verified_steps',1)->get();
        return response()->json(['instructor'=>$instructor]);
    }
    public function verified_Data(Request $request){
        $verified_data=$request->all();
        $id=$request['id'];
        $data=MerchantDetail::where('user_id',$id)->update(['verified_data' => json_encode($verified_data)]);
        return response()->json(['verified_data'=>$data]);
    }
    public function approvedInstructor($id){
        $approved=MerchantDetail::where('user_id',$id)->update(['verified_steps'=>2]);
        return response()->json([
            'approved'=> $approved
        ]);
    }
    public function declinedInstructor($id){
        $declined=MerchantDetail::where('user_id',$id)->update(['verified_steps'=>3]);
        return response()->json([
            'declined'=> $declined
        ]);
    }
    public function suspendInstructor($id){
        $suspend=MerchantDetail::where('user_id',$id)->update(['verified_steps'=>4]);
        return response()->json([
            'suspend'=> $suspend
        ]);
    }
    public function getApproved(){
        $data=MerchantDetail::join('users','users.id','=','merchant.user_id')->where('verified_steps',2)->get();
        return response()->json(['approved'=>$data]);
    }
    public function getDeclined(){
        $data=MerchantDetail::join('users','users.id','=','merchant.user_id')->where('verified_steps',3)->get();
        return response()->json(['declined'=>$data]);
    }
    public function getSuspend(){
        $data=MerchantDetail::join('users','users.id','=','merchant.user_id')->where('verified_steps',4)->get();
        return response()->json(['suspend'=>$data]);
   }
   public function getCount(){
        $approved=MerchantDetail::where('verified_steps',2)->count();
        $declined=MerchantDetail::where('verified_steps',3)->count();
        $suspend=MerchantDetail::where('verified_steps',4)->count();
        return response()->json(['approved'=>$approved,'declined'=>$declined,'suspend'=>$suspend]);
   }
}
