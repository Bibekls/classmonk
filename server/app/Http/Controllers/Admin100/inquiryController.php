<?php

namespace App\Http\Controllers\Admin100;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Message;
use DB;
class inquiryController extends Controller
{
      public function getInquiry(Request $request){

      $inquiry=collect(DB::select("SELECT b.*, concat(usr1.first_name, ' ', usr1.last_name) as Receiver ,usr1.id as reciver_id
                                    FROM (SELECT pdt.name as class_name,pdt.id as product_id ,msg.message,usr.id as sender_id ,concat(usr.first_name, ' ', usr.last_name) as Sender , pdt.user_id from messages msg
                                    INNER JOIN (SELECT min(id) as id from messages group by product_id) as a on a.id = msg.id 
                                    INNER JOIN users usr on msg.from = usr.id
                                    INNER JOIN products pdt on pdt.id = msg.product_id
                                    where msg.seen = 0 AND msg.deleted_at is Null) b 
                                    inner join users usr1 on b.user_id = usr1.id"));
                                    $perPage =10; 
                                    $currentPage =$request['page']-1;
                                    $fata=$currentPage*$perPage;
                                    $pagedData = $inquiry->slice($currentPage*$perPage,$perPage);
                                    $collection= new \Illuminate\Pagination\Paginator($pagedData, count($inquiry),$currentPage);
                  return response()->json(['inquiry'=>$collection,'page'=>$request['page']]);
           }
    public function getSeenInquiry(Request $request){
    $seeninquiry=collect(DB::select("SELECT b.*, concat(usr1.first_name, ' ', usr1.last_name) as Receiver ,usr1.id as reciver_id
                                    FROM (SELECT pdt.name as class_name,pdt.id as product_id ,msg.message,usr.id as sender_id ,concat(usr.first_name, ' ', usr.last_name) as  Sender , pdt.user_id from messages msg
                                    INNER JOIN (SELECT min(id) as id from messages group by product_id) as a on a.id = msg.id 
                                    INNER JOIN users usr on msg.from = usr.id
                                    INNER JOIN products pdt on pdt.id = msg.product_id
                                    where msg.seen = 1 AND msg.deleted_at is Null) b 
                                    inner join users usr1 on b.user_id = usr1.id"));
                                    $perPage =10; 
                                    $currentPage =$request['page']-1;
                                    $fata=$currentPage*$perPage;
                                    $pagedData = $seeninquiry->slice($currentPage*$perPage,$perPage);
                                    $collection= new \Illuminate\Pagination\Paginator($pagedData, count($seeninquiry),$currentPage);
                              return response()->json(['seeninquiry'=>$collection]);
                 }
    public function viewMessage(Request $request){
                                    $sender_id=$request['senderId'];
                                    $reciverId=$request['reciverId'];
                                    $productId=$request['productId'];
            $viewmessage=DB::select(DB::raw("SELECT a.*,usr.email,usr.phone,usr.is_merchant as is_value,usr.profile_pic, concat(usr.first_name, ' ', usr.last_name) as student from (SELECT products.name, messages.message, messages.`from`, messages.`to`, users.first_name, messages.product_id FROM `users`
                                    INNER JOIN products ON users.id = products.user_id 
                                    and products.id = $productId
                                    INNER JOIN messages 
                                    ON messages.product_id = products.id 
                                    and (messages.`from` = $sender_id and `to` = $reciverId) or (`from` = $reciverId and `to` = $sender_id)) a
                                    inner join users usr on usr.id = $sender_id"));
                          return response()->json(['viewmessage'=>$viewmessage]);
           }
      public function markAsSpam(Request $request){
                                    $sender_id=$request['senderId'];
                                    $reciverId=$request['reciverId'];
                                    $productId=$request['productId'];
                                    $deleted=DB::table('messages')->whereRaw("product_id = $productId and ((`FROM` = $sender_id AND `TO` = $reciverId) or (`from` = $reciverId and `TO` = $sender_id))")->update(['deleted_at'=>now()]);
                         return response()->json(['deleted'=>$deleted]);
       } 
      public function MarkAsSpamData(Request $request){
            $markasSpma=collect(DB::select("SELECT b.*, concat(usr1.first_name, ' ', usr1.last_name) as Receiver ,usr1.id as reciver_id
                                    FROM (SELECT pdt.name as class_name,pdt.id as product_id ,msg.message,usr.id as sender_id ,concat(usr.first_name, ' ', usr.last_name) as  Sender , pdt.user_id from messages msg
                                    INNER JOIN (SELECT min(id) as id from messages group by product_id) as a on a.id = msg.id 
                                    INNER JOIN users usr on msg.from = usr.id
                                    INNER JOIN products pdt on pdt.id = msg.product_id
                                    where  msg.deleted_at is not NULL) b 
                                    inner join users usr1 on b.user_id = usr1.id"));
                                    $perPage =10; 
                                    $currentPage =$request['page']-1;
                                    $fata=$currentPage*$perPage;
                                    $pagedData = $markasSpma->slice($currentPage*$perPage,$perPage);
                                    $collection= new \Illuminate\Pagination\Paginator($pagedData, count($markasSpma),$currentPage);
             return response()->json(['mark'=>$collection]);
        }     
   }
