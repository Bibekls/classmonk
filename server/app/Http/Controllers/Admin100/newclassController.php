<?php

namespace App\Http\Controllers\Admin100;
use App\Model\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class newclassController extends Controller
{
    public function getNewClasses(Product $product,Request $request){
        $page=$request['page'];
        $newclass=$product->with('owner')->whereNull('verified_by')->orderBy('id')->paginate(10);
        return response()->json(['newclass'=>$newclass]);
    }
    public function productIdapproved($id){
        $approved=Product::where('id',$id)->update(['verified_by' => $id]);
        return response()->json(['approved'=>$approved]);
    }
    public function productIddeclined($id){
        $declined=Product::where('id',$id)->update(['verified_by' => -1]);
        return response()->json(['declined'=>$declined]);
    }
    public function declinedClass(Product $product,Request $request){
        $page=$request['page'];
        $declined=$product->with('owner')->where('verified_by',-1)->orderBy('id')->paginate(10);
        return response()->json(['declined'=>$declined]);
     }
    public function approvedClass(Product $product,Request $request){
        $page=$request['page'];
        $approved=$product->with('owner')->where('verified_by','!=',-1)->where('verified_by','!=',null)->paginate(10);
        return response()->json(['approved'=>$approved]);
     }
}
