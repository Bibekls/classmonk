<?php

namespace App\Http\Controllers\Admin100;

use GuzzleHttp\Client;
use App\Model\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoginController extends Controller
{
    public function login(Request $request){
        $http = new Client;
        $response = $http->post('http://classmonkserver.com/oauth/token', [
            'form_params' => [
                'grant_type' => 'password',
                'client_id' =>'4',
                'client_secret' => '5Ti9dNdjuUe2Yb13lcEARYNEne7GDJka4tLh1Sh0',
                'username' => $request->input("email"),
                'password' => $request->input("password"),
            ],
        ]);
        return [
            "access_token" => json_decode((string)$response->getBody(), true)['access_token'],
            "user" => json_decode(
                \App\Model\Admin::with('menus')
                    ->where('email', $request->input('email'))
                    ->select('email', 'phoneNumber', 'id','firstname')
                    ->first())
         ];
    }
}
