<?php

namespace App\Http\Controllers\Instructor;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InstructorController extends Controller
{
    public function getInfo(Request $request)
    {
        $user = User::with("merchant.coverImage","merchant.galleryImage")->where("username", $request->input("merchant_name"))->get();
        return $user;
    }
}
