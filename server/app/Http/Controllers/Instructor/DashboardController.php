<?php

namespace App\Http\Controllers\Instructor;

use App\Model\ShiftBook;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    public function getBookedShift()
    {
        $products = Auth::user()->productsWithBookedList;

        $finalProduct = collect([]);

        foreach ($products as $product) {
            $hasBooking = false;
            $classShiftArray=collect([]);
            foreach ($product->classShifts as $classShiftKey=> $classShift) {
                if ($classShift->shiftBook->count() > 0) {
                    $hasBooking = true;
                    $classShiftArray->push($classShift);
                }
                unset($product->classShifts[$classShiftKey]);
            }
            if ($hasBooking) {
                $product->_class_shifts = $classShiftArray ;
                $finalProduct->push($product);
            }
        }

        return $finalProduct;

    }
}
