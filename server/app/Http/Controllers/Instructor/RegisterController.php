<?php

namespace App\Http\Controllers\Instructor;

use App\Model\MerchantDetail;
use App\Model\ProfileImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class RegisterController extends Controller
{
    private $imageServer;

    public function __construct()
    {
        $this->imageServer = public_path();
    }

    public function slugify($text)
    {
        $text = preg_split('/[^[:alnum:]]+/', $text);
        $finalText = "";
        foreach ($text as $value) {
            if ($finalText == "") $finalText = $value;
            else $finalText = $finalText . "-" . $value;
        }
        return $finalText;
    }

    public function register(Request $request)
    {
        $slug = $this->slugify($request->input('firstName') . '-' . $request->input('lastName'));
        $finalSlug = (count(MerchantDetail::withTrashed()->where('slug', $slug)->get()) > 0) ? strtolower($slug . '-' . str_random(6)) : strtolower($slug);
        if ($finalSlug === "") {
            $finalSlug = time();
        }

        $user = User::create([
            'first_name' => $request->input('firstName'),
            'last_name' => $request->input('lastName'),
            'email' => $request->input('email'),
            'street_address' => $request->input('address'),
            'mobile_number' => $request->input('phone'),
            'password' => bcrypt($request->input('password')),
        ]);

        $user->merchant()->create([
            'merchant_type' => $request->input('merchantType'),
            'phone' => $request->input('phone'),
            'website' => $request->input('website'),
            'slug' => $finalSlug,
            'descripton' => $request->input('description')
        ]);


        $productImage = $request->file('profileImage');

        if ($productImage) {

            $filename = uniqid() . $productImage->getClientOriginalName();
            $filename = preg_replace('/\s+/', '_', $filename);
            if (!file_exists($this->imageServer . '/images/profile-pic')) {
                mkdir($this->imageServer . '/images/profile-pic', 0777, true);
            }
            $productImage->move($this->imageServer . '/images/profile-pic', $filename);

            $image = ProfileImage::create([
                'user_id' => $user->id,
                'file_name' => $filename,
                'file_size' => $productImage->getClientSize(),
                'file_mime' => $productImage->getClientMimeType(),
                'file_path' => $this->imageServer . '/images/profile-pic/' . $filename,
                'is_cover_image' => true,
            ]);

            $user->update(['profile_pic' => $image->id]);
            $user->merchant()->update(['cover_image' => $image->id]);

            if (!file_exists($this->imageServer . '/images/profile-pic/thumbnails')) {
                mkdir($this->imageServer . '/images/profile-pic/thumbnails', 0777, true);
            }

            try {
                Image::make($this->imageServer . '/images/profile-pic/' . $filename)
                    ->fit(64, 64)->save($this->imageServer . '/images/profile-pic/thumbnails/' . $filename, 50);
            } catch (Exception $e) {
                Log::info($e->getMessage());
            }
        }

        Auth::loginUsingId($user->id, true);

        return response()->json([
            "message" => "success"
        ]);

        //return response()->json(['success'=>true,'path'=>'/'.$product->owner->city->name.'/'.$product->category->name.'/'.$product->slug]);

//        $http = new Client();
//        $response = $http->post('http://local-server.classmonk.com/oauth/token', [
//            'form_params' => [
//                'grant_type' => 'password',
//                'client_id' => '2',
//                'client_secret' => env('APP_CLIENT'),
//                'username' => $request->input("email"),
//                'password' => $request->input("password"),
//                //'redirect_uri' => 'http://example.com/callback',
//                //'code' => $request->code,
//            ],
//        ]);
//
//        return [
//            "access_token" => json_decode((string)$response->getBody(), true)['access_token'],
//            "user" => json_decode(
//                User::with('merchant.coverImage', 'profileImage', 'product_exist')
//                    ->where('email', $request->input('email'))
//                    ->select('email', 'phone', 'id', 'is_merchant')
//                    ->first())
//        ];
    }
}
