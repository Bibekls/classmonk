<?php

namespace App\Http\Controllers\Instructor;

use App\Model\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notification;
use App\Model\ShiftBook;

class NotificationController extends Controller
{
    public function acceptBookingNotification(Request $request){

        $notification = Notification::find($request->input('id'));

        $shiftBook=ShiftBook::create([
            'class_shift_id'=>$notification->shift_id,
            'no_of_seat'=>$notification->no_of_seat,
            'accept'=>true,
            'date'=>$notification->date,
            'booked_by'=>$notification->created_by
        ]);

        Message::create([
            "message"=>"Your request is approved",
            "product_id"=>$notification->product_id,
            "from"=>$notification->owner->id,
            "to"=>$notification->created_by,
            "seen"=>0
        ]);

        $notification->update([
            'action'=>1
        ]);

        return ShiftBook::with('shift.product.owner')->find($shiftBook->id);
    }

    public function declineBookingNotification(Request $request){
        return $request->all();
    }
}
