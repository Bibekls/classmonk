<?php

namespace App\Http\Controllers\Instructor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Product;
use Illuminate\Support\Facades\Auth;
use App\Model\Message;
use App\User;


class MessageController extends Controller
{
    public function index(Request $request)
    {

        $firstProduct = Auth::user()->products->where('slug', $request->input('product'))->first();
        if ($firstProduct == null) {
            $otherProduct = Product::where('slug', $request->input('product'))->first();

            if ($otherProduct) {

                $conversation = Message::where('product_id', $otherProduct->id)
                    ->where(function ($query) use ($request) {
                        $query->where(function ($query) use ($request) {
                            $query->where('from', Auth::user()->id)
                                ->where('to', $request->input('from'));
                        })
                            ->orWhere(function ($query) use ($request) {
                                $query->where('from', $request->input('from'))
                                    ->where('to', Auth::user()->id);
                            });
                        // $query->where('from',Auth::user()->id)
                        //         ->where('to',$request->input('id'));
                    })
                    ->get();

                foreach ($conversation as $singleMSG) {
                    if ($singleMSG->to == Auth::user()->id) $singleMSG->seen = 1;
                    $singleMSG->save();
                }
            }

            return view('member.message.message')
                ->with('title', 'Message')
                ->with('messages', null)
                ->with('product', $otherProduct)
                ->with('otherProduct', true);
        }

        $conversation = Message::where('product_id', $firstProduct->id)
            ->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('from', Auth::user()->id)
                        ->where('to', $request->input('from'));
                })
                    ->orWhere(function ($query) use ($request) {
                        $query->where('from', $request->input('from'))
                            ->where('to', Auth::user()->id);
                    });
                // $query->where('from',Auth::user()->id)
                //         ->where('to',$request->input('id'));
            })
            ->get();

        foreach ($conversation as $singleMSG) {
            if ($singleMSG->to == Auth::user()->id) $singleMSG->seen = 1;
            $singleMSG->save();
        }


        $messages = array();
        if ($firstProduct != null) {
            $senderIds = Message::where('from', Auth::user()->id)->groupBy('to')->pluck('to')->toArray();
            $receiverIds = Message::where('to', Auth::user()->id)->groupBy('from')->pluck('from')->toArray();

            $connectedUserIds = array_merge($senderIds, $receiverIds);
            $connectedUserIds = array_unique($connectedUserIds);

            foreach ($connectedUserIds as $connectedUserId) {
                //Receiver is auth user
                $messageHistory = array();
                $messageHistory['member'] = User::find($connectedUserId);


                $send = Message::where('from', $connectedUserId)->where('to', Auth::user()->id)->where('product_id', $firstProduct->id)->get();
                //Sender is auth user
                $receive = Message::where('to', $connectedUserId)->where('from', Auth::user()->id)->where('product_id', $firstProduct->id)->get();

                $final = $receive->toBase()->merge($send)->sortBy('created_at');

                $existUnreadMessage = false;
                foreach ($final as $singleMessage) {
                    if ($singleMessage->seen == 0 && $singleMessage->to == Auth::user()->id) {
                        $existUnreadMessage = true;
                    }
                }

                if ($existUnreadMessage) {
                    $messageHistory['unreadThread'] = 1;
                } else {
                    $messageHistory['unreadThread'] = 0;
                }

                $messageHistory['messages'] = $final;
                array_push($messages, $messageHistory);
            }
        }

        $messagesArray = (array)$messages;

        usort($messagesArray, function ($a, $b) {

            if (count($a["messages"]) > 0) {
                $highestA = strtotime(collect($a["messages"])->take(-1)->first()->created_at);
            } else {
                $highestA = 0;
            }
            if (count($b["messages"]) > 0) {
                $highestB = strtotime(collect($b["messages"])->take(-1)->first()->created_at);
            } else {
                $highestB = 0;
            }

            if ($highestA == $highestB) {
                return 0;
            }

            if ($highestA == $highestB) {
                return 0;
            }
            return ($highestA > $highestB) ? -1 : 1;
        });

//        foreach($messagesArray as $singleMessage){
//            echo json_encode($singleMessage);
//            echo '<hr>';
//        }
//


        return view('member.message.message')
            ->with('title', 'Message')
            ->with('messages', $messagesArray)
            ->with('product', $firstProduct)
            ->with('otherProduct', false);
    }

    public function show($id)
    {
        $messages = Auth::user()->inbox()->paginate(10);
        $parseMessage = json_decode(json_encode($messages));

        if ($id === "load-message-dropdown") {
            $view = view('member.message.message-dropdown-component')->with('messages', $messages);
            $messageComponent = $view->render();
            return response()->json([
                'messageComponent' => $messageComponent,
                'next_page_url' => $parseMessage->next_page_url,
                'current_page' => $parseMessage->current_page,
                'last_page' => $parseMessage->last_page,
            ]);
        }
        //return view('member.message.message-dropdown-component');
    }

    public function store(CreateMessageRequest $request)
    {
        Message::create([
            'from' => Auth::user()->id,
            'to' => $request->input('id'),
            'product_id' => $request->input('product-id'),
            'message' => $request->input('message'),
            'seen' => '0'
        ]);

        $title = 'You have message from ' . Auth::user()->first_name;
        $receiverEmail = User::find($request->input('id'))->email;

        Mail::send('emails.member.message-email', ['to' => User::find($request->input('id'))->first_name,
            'from' => Auth::user()->first_name,
            'product' => Product::find($request->input('product-id'))->name,
            'coverImage' => Product::find($request->input('product-id'))->coverImage->file_name,
            'emailMessage' => $request->input('message')
        ], function ($message) use ($title, $receiverEmail) {
            $message->from('noreply@classmonk.com', 'Classmonk');
            $message->subject($title);
            $message->to($receiverEmail);
        });


        if (!$request->ajax()) {
            return redirect('/member/message');
        }

        $conversation = Message::where('product_id', $request->input('product-id'))
            ->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('from', Auth::user()->id)
                        ->where('to', $request->input('id'));
                })
                    ->orWhere(function ($query) use ($request) {
                        $query->where('from', $request->input('id'))
                            ->where('to', Auth::user()->id);
                    });
                // $query->where('from',Auth::user()->id)
                //         ->where('to',$request->input('id'));
            })
            ->get();
        foreach ($conversation as $singleMSG) {
            if ($singleMSG->to == Auth::user()->id) $singleMSG->seen = 1;
            $singleMSG->save();
        }

        $view = view('member.message.conversation-component')->with('conversation', $conversation)->with('product', Product::find($request->input('product-id')));
        $conversationComponent = $view->render();
        return response()->json([
            'conversationComponent' => $conversationComponent,
        ]);

    }

    public function seeAllConversation(Request $request)
    {
        if (!$request->ajax()) {
            return redirect('/member/message');
        }

        $conversation = Message::where('product_id', $request->input('product-id'))
            ->where(function ($query) use ($request) {
                $query->where(function ($query) use ($request) {
                    $query->where('from', Auth::user()->id)
                        ->where('to', $request->input('id'));
                })
                    ->orWhere(function ($query) use ($request) {
                        $query->where('from', $request->input('id'))
                            ->where('to', Auth::user()->id);
                    });
                // $query->where('from',Auth::user()->id)
                //         ->where('to',$request->input('id'));
            })
            ->get();
        foreach ($conversation as $singleMSG) {
            if ($singleMSG->to == Auth::user()->id) $singleMSG->seen = 1;
            $singleMSG->save();
        }
        return response()->json([
            'status' => 'Seen',
            'conversation' => $conversation
        ]);
    }

    public function loadMessage(Request $request)
    {

        $firstProduct = Product::where('slug', $request->input('slug'))->first();
        if (!$firstProduct) return response()->json(["error" => "Product not found"]);

        $senderIds = Message::where('from', Auth::user()->id)->groupBy('to')->pluck('to')->toArray();
        $receiverIds = Message::where('to', Auth::user()->id)->groupBy('from')->pluck('from')->toArray();

        $connectedUserIds = array_merge($senderIds, $receiverIds);
        $connectedUserIds = array_unique($connectedUserIds);
        $messages = array();

        foreach ($connectedUserIds as $connectedUserId) {
            //Receiver is auth user
            $messageHistory = array();
            $messageHistory['member'] = User::with('profileImage')->find($connectedUserId);

            $send = Message::where('from', $connectedUserId)->where('to', Auth::user()->id)->where('product_id', $firstProduct->id)->get();
            //Sender is auth user
            $receive = Message::where('to', $connectedUserId)->where('from', Auth::user()->id)->where('product_id', $firstProduct->id)->get();

            $final = $receive->toBase()->merge($send)->sortBy('created_at');

            $existUnreadMessage = false;
            foreach ($final as $singleMessage) {
                if ($singleMessage->seen == 0 && $singleMessage->to == Auth::user()->id) {
                    $existUnreadMessage = true;
                }
            }
            if ($existUnreadMessage) {
                $messageHistory['unreadThread'] = 1;
            } else {
                $messageHistory['unreadThread'] = 0;
            }

            $messageHistory['messages'] = $final;
            array_push($messages, $messageHistory);
        }

        // foreach ($messages as $message) {
        //  echo $message['member']->first_name.' <br> ';
        //  foreach ($message['messages'] as $singleMessage) {
        //      echo $singleMessage->message . ' <br> ';
        //  }
        // }

        $messagesArray = (array)$messages;

        usort($messagesArray, function ($a, $b) {
            if (count($a["messages"]) > 0) {
                $highestA = strtotime(collect($a["messages"])->take(-1)->first()->created_at);
            } else {
                $highestA = 0;
            }
            if (count($b["messages"]) > 0) {
                $highestB = strtotime(collect($b["messages"])->take(-1)->first()->created_at);
            } else {
                $highestB = 0;
            }

            if ($highestA == $highestB) {
                return 0;
            }
            return ($highestA > $highestB) ? -1 : 1;
        });


        $sortedMessage=collect([]);
        foreach ($messagesArray as $key => $messageComponent) {
            $sortedMessage=collect([]);
            foreach ($messageComponent['messages'] as $secondKey=>$message){
                $sortedMessage->push($message);
            }
            unset($messagesArray[$key]['messages']);
            $messagesArray[$key]['messages']=$sortedMessage;
        }

        return response()->json([
            'messages' => $messagesArray,
        ]);

    }

    public function loadOtherMessage(Request $request)
    {
        $productListArray = array();

        $productId = Product::where('slug', $request->input('slug'))->first()->id;
        $productListArray[0] = $productId;
//        foreach(Auth::user()->productToCart() as $productList){
//            array_push($productListArray,$productList->id);
//        }

        $senderIds = Message::where('from', Auth::user()->id)->groupBy('to')->pluck('to')->toArray();
        $receiverIds = Message::where('to', Auth::user()->id)->groupBy('from')->pluck('from')->toArray();

        $connectedUserIds = array_merge($senderIds, $receiverIds);
        $connectedUserIds = array_unique($connectedUserIds);
        $messages = array();

        foreach (array_keys($productListArray) as $productListCounter) {

            //Receiver is auth user
            $messageHistory = array();
            $messageHistory['product'] = Product::find($productListArray[$productListCounter]);


            $send = Message::where('from', $messageHistory['product']->owner->id)->where('to', Auth::user()->id)->where('product_id', $productListArray[$productListCounter])->get();
            //Sender is auth user
            $receive = Message::where('to', $messageHistory['product']->owner->id)->where('from', Auth::user()->id)->where('product_id', $productListArray[$productListCounter])->get();

            $final = $receive->toBase()->merge($send)->sortBy('created_at');

            $existUnreadMessage = false;
            foreach ($final as $singleMessage) {
                if ($singleMessage->seen == 0 && $singleMessage->to == Auth::user()->id) {
                    $existUnreadMessage = true;
                }
            }

            if ($existUnreadMessage) {
                $messageHistory['unreadThread'] = 1;
            } else {
                $messageHistory['unreadThread'] = 0;
            }

            $messageHistory['messages'] = $final;
            array_push($messages, $messageHistory);

        }


        // foreach ($messages as $message) {
        //  echo $message['member']->first_name.' <br> ';
        //  foreach ($message['messages'] as $singleMessage) {
        //      echo $singleMessage->message . ' <br> ';
        //  }
        // }

        $messagesArray = (array)$messages;

        usort($messagesArray, function ($a, $b) {

            if (count($a["messages"]) > 0) {
                $highestA = strtotime(collect($a["messages"])->take(-1)->first()->created_at);
            } else {
                $highestA = 0;
            }
            if (count($b["messages"]) > 0) {
                $highestB = strtotime(collect($b["messages"])->take(-1)->first()->created_at);
            } else {
                $highestB = 0;
            }

            if ($highestA == $highestB) {
                return 0;
            }


            if ($highestA == $highestB) {
                return 0;
            }
            return ($highestA > $highestB) ? -1 : 1;
        });

        $sortedMessage=collect([]);
        foreach ($messagesArray as $key => $messageComponent) {
            foreach ($messageComponent['messages'] as $secondKey=>$message){
                $sortedMessage->push($message);
            }
            unset($messagesArray[$key]['messages']);
            $messagesArray[$key]['messages']=$sortedMessage;
        }


        return response()->json([
            'messages' => $messagesArray,
        ]);

    }
}
