<?php

namespace App\Http\Controllers\Instructor;

use App\Model\Category;
use App\Model\City;
use App\Model\Product;
use App\Model\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use Intervention\Image\Facades\Image;

class ProductController extends Controller
{

    private $imageServer;

    public function __construct()
    {
        $this->imageServer = public_path();
    }

    public function slugify($text)
    {
        $text = preg_split('/[^[:alnum:]]+/', $text);
        $finalText = "";
        foreach ($text as $value) {
            if ($finalText == "") $finalText = $value;
            else $finalText = $finalText . "-" . $value;
        }
        return $finalText;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Auth::user()->products;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = json_decode($request->input('data'));

        $MAX_COUNT = 6;
        $count = 0;

        $slug = $this->slugify($data->title);
        $finalSlug = (count(Product::withTrashed()->where('slug', $slug)->get()) > 0) ? strtolower($slug . '-' . str_random(6)) : strtolower($slug);
        if ($finalSlug === "") {
            $finalSlug = time();
        }

        $category = Category::where('name', $data->category)->first();
        if ($category)
            $categoryId = $category->id;


        $selectedAmenities = [];
        $counter = 0;
        if($data->amenities)
        foreach ($data->amenities as $amenity) {
            if ($data->weProvide[$counter] == "true") {
                array_push($selectedAmenities, $amenity);
            }
            $counter++;
        }

        $product = Product::create([
            "category_id" => $categoryId,
            "user_id" => Auth::user()->id,
            "name" => $data->title,
            "slug" => $finalSlug,
//            "final_price" => $data->pricing,
            "view_count" => 0,
            //"description" => $data->description,
            "is_available" => true,
            "other_features" => null,
            "wsjtc" => $data->whoCanJoin,
            "hlatc" => $data->classLength,
            "amenities" => json_encode($selectedAmenities),
//            "total_seat" => $data->seatAmount,
            "class_type" => $data->classType,
            "location" => $data->location
        ]);

        if ($data->targetCity)
            foreach ($data->targetCity as $city) {
                $cityRecord = City::where('name', $city->value)->first();

                if ($cityRecord)
                    $product->cities()->create([
                        'city_id' => $cityRecord->id
                    ]);
            }

        if ($data->classType == 1) {
            foreach ($data->packageClass as $packageClass) {
                $classPackage = $product->classPackages()->create([
                    "start_time" => date("H:i", strtotime($packageClass->startTime)),
                    "end_time" => date("H:i", strtotime($packageClass->endTime)),
                    "seat" => $packageClass->seatAmount,
                    "session" => count($packageClass->dateRange)?count($packageClass->dateRange):$packageClass->session,
                    "pricing" => $packageClass->pricing,
                ]);

                foreach ($packageClass->dateRange as $date) {
                    $classPackage->packageDates()->create([
                        "date" => substr($date,0,10)
                    ]);
                }
            }
        }

        if ($data->classType == 2) {
            foreach ($data->scheduleClass as $scheduleClass) {
                $product->classShifts()->create([
                    "start_time" => date("H:i", strtotime($scheduleClass->startTime)),
                    "end_time" => date("H:i", strtotime($scheduleClass->endTime)),
                    "seat" => $scheduleClass->seatAmount,
                    "sun" => $scheduleClass->sun,
                    "mon" => $scheduleClass->mon,
                    "tue" => $scheduleClass->tue,
                    "wed" => $scheduleClass->wed,
                    "thu" => $scheduleClass->thu,
                    "fri" => $scheduleClass->fri,
                    "sat" => $scheduleClass->sat,
                    "pricing" => $scheduleClass->pricing
                ]);
            }
        }

        if ($request->file('product-images'))
            foreach ($request->file('product-images') as $productImage) {
                if ($count < $MAX_COUNT) {

                    $filename = uniqid() . $productImage->getClientOriginalName();
                    if (!file_exists($this->imageServer . '/images/product-image')) {
                        mkdir($this->imageServer . '/images/product-image', 0777, true);
                    }
                    $productImage->move($this->imageServer . '/images/product-image', $filename);

                    $image = ProductImage::create([
                        'product_id' => $product->id,
                        'file_name' => $filename,
                        'file_size' => $productImage->getClientSize(),
                        'file_mime' => $productImage->getClientMimeType(),
                        'file_path' => $this->imageServer . '/images/product-image/' . $filename,
                        'is_cover_image' => ($count == 0) ? true : false,
                    ]);


                    if (!file_exists($this->imageServer . '/images/product-image/small')) {
                        mkdir($this->imageServer . '/images/product-image/small', 0777, true);
                    }

                    Image::make($this->imageServer . '/images/product-image/' . $filename)
                        ->resize(800, null, function ($constraint) {
                            $constraint->aspectRatio();
                        })->save($this->imageServer . '/images/product-image/small/' . $filename, 100);


                    $count++;
                }
            }

        return response()->json([
            'url' => $cityRecord->slug . '/' . $category->slug . '/' . $product->slug
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
