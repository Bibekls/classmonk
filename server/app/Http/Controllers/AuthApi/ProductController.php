<?php

namespace App\Http\Controllers\AuthApi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Notification;
use App\Model\ClassShift;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    function applyForShift(Request $request)
    {
        $shift=ClassShift::find($request->input('shiftId'));

        if($shift){

            Notification::create([
                'user_id'=>$shift->product->owner->id,
                'product_id'=>$shift->product->id,
                'type'=>'2',
                'shift_id'=>$shift->id,
                'no_of_seat'=>$request->input('totalSeat'),
                'title'=>'Shift book request',
                'description'=>'shift book request',
                'created_by'=>Auth::id(),
                'action'=>'0'
            ]);

            return response()->json([
                "status"=>"Success"
            ]);

        }
    }

    function getProductInConversation(){
        if (Auth::user()->is_merchant){
            return Auth::user()->myProductInConversation();

        }else{
            return Auth::user()->productInConversation();
        }
    }
}
