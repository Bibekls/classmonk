<?php

namespace App\Http\Controllers\AuthApi;

use App\Model\Notification;
use App\Model\ShiftBook;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class NotificationController extends Controller
{
    public function getNotification(){
        return Auth::user()->notifications;
    }


}
