<?php

namespace App\Http\Controllers\AuthApi;

use App\Events\MessageCreated;
use App\Model\Message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    protected  function sendMessage(Request $request){

        if(Auth::id()==$request->input('to'))
            return response()->json([
                "message"=>"Sender and receiver cant be same"
            ]);

        $message = Message::create([
            'from'=>Auth::id(),
            'to'=>$request->input('to'),
            'product_id'=>$request->input('product_id'),
            'message'=>$request->input('message'),
            'seen'=>0
        ]);

        broadcast(new MessageCreated($message,$request->input('thread')))->toOthers();

        return $message;
    }
}
