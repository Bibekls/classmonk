<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Model\Product;

class Applicant extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id','user_id','product_id','name','email','phone','cover_letter','move_date','document_files','others'
    ];

    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
