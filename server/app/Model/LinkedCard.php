<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LinkedCard extends Model
{
    protected $fillable = [
        'id',
        'user_id',
        'card_number',
        'transaction_tag',
        'authorization_number',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
