<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PackageDate extends Model
{
    protected $fillable = [
        'package_class_id', 'date'
    ];
}