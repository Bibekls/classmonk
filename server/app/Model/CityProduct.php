<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CityProduct extends Model
{
    protected $fillable = ['product_id','city_id'];

    public function product(){
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function city(){
        return $this->belongsTo(City::class);
    }

}
