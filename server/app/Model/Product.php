<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Category;
use App\Model\Tag;
use App\User;
use App\Model\ProductComment;
use App\Model\ProductAddress;
use App\Model\WishlistDetail;
use App\Model\Message;
use App\Model\JobCategory;
use App\Model\Notification;

use Auth;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'category_id','sub_category_id','user_id','name','slug','final_price','view_count','description','wsjtc','hlatc','amenities','total_seat','class_type','location','is_available','other_features','target_university','available_from','sold_to_user_id','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
	Methods
    */


    public function images()
    {
        return $this->hasMany(ProductImage::class,'product_id','id');
    }
    
    public function coverImage()
    {
        return $this->hasOne(ProductImage::class)->where('is_cover_image',true);
    }

    public function category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
   

    public function owner()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function classPackages()
    {
        return $this->hasMany(ClassPackage::class);
    }


    public function classShifts()
    {
        return $this->hasMany(ClassShift::class);
    }

    public function cities(){
        return $this->hasMany(CityProduct::class);
    }

    public function soldTo()
    {
        return $this->belongsTo(User::class,'sold_to_user_id','id');
    }

    public function wisher()
    {
        return $this->hasMany(WishlistDetail::class,'product_id','id');
    }

    public function customer()
    {
        $message=Message::where('product_id',$this->id)->pluck('from')->toArray();
        $user=User::whereIn('id',$message)->where('id','<>',$this->owner->id)->get();
        return $user;
    }
    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function comments(){
        return $this->hasMany(ProductComment::class)->orderBy('created_at', 'desc');
    }

    public function firstFiveComments(){
        return $this->hasMany(ProductComment::class)->where('reply_to',null)->orderBy('created_at', 'desc')->limit(5);
    }


    public function commentsOnly(){
        return $this->hasMany(ProductComment::class)->where('reply_to',null)->orderBy('created_at', 'desc');
    }

    public function address(){
         return $this->hasOne(ProductAddress::class);
    }

    public function attachedMessages(){
         return $this->hasMany(Message::class);
    }

    public function jobCategory(){
        return $this->belongsTo(JobCategory::class);
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }

    public function wishlists(){
        return $this->hasMany(WishlistDetail::class);
    }
    public function notifications(){
        return $this->hasMany(Notification::class);
    }
    public function scopePublished($query)
    {
        return $query->where('published_at', '!=', 0);
    }

    public function msgUnreadThread(){
        $senderIds=Message::where('from',Auth::user()->id)->groupBy('to')->pluck('to')->toArray();
        $receiverIds=Message::where('to',Auth::user()->id)->groupBy('from')->pluck('from')->toArray();
        $connectedUserIds=array_merge($senderIds,$receiverIds);
        $connectedUserIds=array_unique($connectedUserIds);
        $messages= array();
        $unreadThread=0;
        foreach ($connectedUserIds as $connectedUserId)
        {
            //Receiver is auth user
            $messageHistory=array();
            $messageHistory['member']=User::find($connectedUserId);


            $send=Message::where('from',$connectedUserId)->where('to',Auth::user()->id)->where('product_id',$this->id)->get();
            //Sender is auth user
            $receive=Message::where('to',$connectedUserId)->where('from',Auth::user()->id)->where('product_id',$this->id)->get();

            $final=$receive->toBase()->merge($send)->sortBy('created_at');
            $existUnreadMessage=false;
            foreach($final as $singleMessage){
                if($singleMessage->seen==0 && $singleMessage->to==Auth::user()->id){
                    $existUnreadMessage=true;
                }
            }
            if($existUnreadMessage)$unreadThread++;

            $messageHistory['messages']=$final;
            array_push($messages,$messageHistory);
        }

        return $unreadThread;
    }

    public function lastMessageWith(){
        $authId = Auth::user()->id;
        return Message::where("product_id",$this->id)->where(function ($query) use ($authId){
                $query->where('from',$authId)->orWhere('to',$authId);
            })->orderBy("created_at","desc")->first();
    }

}
