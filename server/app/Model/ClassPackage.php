<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClassPackage extends Model
{
    protected $fillable = ['id','product_id','start_time','end_time','seat','session','pricing'];

    public function classShifts()
    {
        return $this->hasMany(ClassShift::class);
    }
    public function packageDates(){
        return $this->hasMany(PackageDate::class,'class_package_id');
    }
}
