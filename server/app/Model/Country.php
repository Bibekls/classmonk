<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\University;
class Country extends Model
{
	protected $fillable = [
        'nicename','iso'
    ];

    protected $table='country';
    protected $primaryKey='iso';

    public function universities()
    {
        return $this->hasMany(University::class,'country','iso');
    }
}
