<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
class SocialAccount extends Model
{
    protected $fillable = ['user_id', 'social_user_id', 'signup_media','avatar'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
