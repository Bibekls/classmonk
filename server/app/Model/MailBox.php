<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MailBox extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'from','to','subject','message','owner_deleted_at'
    ];
    protected $table='mailbox';
}
