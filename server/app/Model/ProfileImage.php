<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ProfileImage extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','file_name','file_size','file_mime','file_path','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
	Methods
    */
    public function user()
    {
        return $this->belongsTo('User','user_id','id');
    }
}
