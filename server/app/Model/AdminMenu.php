<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AdminMenu extends Model
{
    protected $fillable = [

    ];
    public function submenus(){
        return $this->hasMany(SubMenu::class,'admin_menus_id','id');
    }   
}
