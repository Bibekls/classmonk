<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','type','title','product_id','comment_id','description','created_by','shift_id','no_of_seat','date','action'
    ];
    protected $dates = ['deleted_at'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function owner()
    {
        return $this->belongsTo(User::class,'user_id');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class,'created_by');
    }

}
