<?php

namespace App\Model;

use Laravel\Passport\HasApiTokens;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table="admins";
    protected $guard ="admin";

    protected $fillable = [
        'firstname','middlename','lastname','address',
        'country','phoneNumber','email','password','transaction_password','admin_level',
        'created_by','updated_by','owner_deleted_at'
    ];
    public function menus(){
        return $this->hasMany(AdminAccessMenu::class,'admin_id','id')->join('admin_menus','admin_menus.id','=','admin_access_menus.admin_menus_id');;
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}

// <?php

// namespace App\Model;

// use Illuminate\Database\Eloquent\Model;

// class Admin extends Model
// {
//     protected $fillable = [
//         'firstname','middlename','lastname','address',
//         'country','phoneNumber','email','password','transaction_password','admin_level',
//         'created_by','updated_by','owner_deleted_at'
//     ];
//     public function menus(){
//         return $this->hasMany(AdminAccessMenu::class,'admin_id','id')->join('admin_menus','admin_menus.id','=','admin_access_menus.admin_menus_id');;
//     }
// }