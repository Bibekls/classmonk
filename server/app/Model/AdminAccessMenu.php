<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\AdminMenu;
class AdminAccessMenu extends Model
{
    protected $fillable = [
         'id','admin_menus_id','admin_id','permission'
    ];
    // public function accessmenus(){
    //     return $this->belongsTo(AdminMenu::class,'admin_menus_id','id') ;       
    //  } 
}
