<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'id',
        'reference',
        'created_by',
        'user_type',
        'email',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
