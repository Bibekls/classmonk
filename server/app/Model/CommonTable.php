<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommonTable extends Model
{
    protected $fillable = ['id','key','value','parent_id'];

    function childs(){
        return $this->hasMany(CommonTable::class,'parent_id');
    }

    function parent(){
        return $this->hasOne(CommonTable::class,'parent_id');
    }
}
