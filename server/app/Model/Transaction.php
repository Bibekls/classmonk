<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'id',
        'linked_card_id',
        'amount',
        'user_id',
        'class_shift_id',
        'log',
        'remark',
        'type',
        'transaction_tag',
        'authorization_number',
        'created_at',
        'updated_at',
        'deleted_at',
    ];
}
