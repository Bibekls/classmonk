<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\User;
use App\Model\Tag;
use App\Model\Product;
use App\Model\CategoryUniversity;
class Category extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'parent_id','name','slug','description',
        'image_url','sort_order','status','is_class',
        'created_by','updated_by','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
	Methods
    */
    public function products()
    {
        return $this->hasMany(Product::class,'category_id','id');
    }

    public function subCategory()
    {
        return $this->hasMany(Category::class,'parent_id','id');
    }

    public function parentCategory() {
        return $this->belongsTo(Category::class, 'parent_id', 'id');
    }

    public function universities()
    {
        return $this->belongsToMany(University::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function createBy()
    {
        return $this->belongsTo('User','created_by','id');
    }

    public function updatedBy()
    {
        return $this->belongsTo('User','updated_by','id');
    }


}
