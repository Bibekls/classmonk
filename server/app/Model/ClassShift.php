<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Product;

class ClassShift extends Model
{
    protected $fillable = ['id','product_id','package_id','start_time','end_time','seat','sun','mon','tue','wed','thu','fri','sat'];

    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }

    public function shiftBook()
    {
        return $this->hasMany(ShiftBook::class);
    }
}
