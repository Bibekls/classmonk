<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Model\Product;
class Message extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'message','product_id','to','from','seen','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
    Methods
    */
    public function receiver()
    {
        return $this->belongsTo(User::class,'to','id');
    }
    public function sender()
    {
        return $this->belongsTo(User::class,'from','id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class,'product_id','id');
    }
}
