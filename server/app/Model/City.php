<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable = ['id','name','slug','is_deleted','address'];

    public function products(){
        return $this->hasMany(CityProduct::class);
    }
}
