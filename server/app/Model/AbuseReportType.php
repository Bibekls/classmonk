<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AbuseReportType extends Model
{
    protected $fillable = [
        'name'
    ];
}
