<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use App\Model\Product;
use App\Model\AbuseReportType;

class AbuseReport extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'report_to_user','report_by_user','subject','comment','abuse_report_type_id','product_id','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
    Methods
    */

    public function from()
    {
        return $this->belongsTo(User::class,'report_by_user','id');
    }

    public function to()
    {
        return $this->belongsTo(User::class,'report_to_user','id');
    }

    public function type()
    {
        return $this->belongsTo(AbuseReportType::class,'abuse_report_type_id','id');
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
