<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
class WishlistDetail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','product_id','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
    Methods
    */
    public function product()
    {
        return $this->belongsTo('model/Product','product_id','id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
