<?php

namespace App\Model;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MerchantDetail extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'user_id','name','merchant_type','slug','address','website','phone','cover_image','description','verified_data','verified_steps'
    ];
    protected $table='merchant';
    //protected $primaryKey = 'slug';

    public function coverImage(){
        return $this->belongsTo(ProfileImage::class,'cover_image','id')
            ->select('file_name','id');
    }

    public function galleryImage(){
        return $this->hasMany(ProfileImage::class,'user_id','user_id')
            ->select('user_id','file_name','id');
    }
    public function getVerifiedDataAttribute($value)
    {
        return json_decode($value,true);
    }
}
