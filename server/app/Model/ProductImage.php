<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ProductImage extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'product_id','file_name','file_size','file_mime','file_path','is_cover_image','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
	Methods
    */
    public function product()
    {
        return $this->belongsTo('model/Product','product_id','id');
    }
}
