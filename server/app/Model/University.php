<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\Category;
use App\Model\Country;
use App\User;

use App\Model\City;

class University extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'name','slug','description','address','postal_code','city_id','state','zone','country','longitude','latitude','cover_image','created_by','updated_by','owner_deleted_at'
    ];

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    /*
    Methods
    */

    public function users()
    {
        return $this->hasMany(User::class,'university_id','id');
    }

    public function gps()
    {
        return $this->belongsTo('model/GPS','gps','id');
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }


    public function countryName()
    {
        return $this->belongsTo(Country::class,'country','iso');
    }
}
