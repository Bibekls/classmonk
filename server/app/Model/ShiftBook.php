<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use App\Model\ClassShift;

class ShiftBook extends Model
{
    use SoftDeletes;
    protected $fillable = [
        'id','class_shift_id','no_of_seat','accept','date','booked_by'
    ];
    protected $dates = ['deleted_at'];

    public function shift()
    {
        return $this->belongsTo(ClassShift::class,'class_shift_id');
    }

    public function bookedBy()
    {
        return $this->belongsTo(User::class,'booked_by','id');
    }
}