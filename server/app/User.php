<?php

namespace App;


use App\Model\MerchantDetail;
use App\Model\ProfileImage;
use App\Model\Product;
use App\Model\Message;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use App\Model\Notification;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'transaction_password', 'phone', 'profile_pic', 'first_name', 'last_name', 'city', 'read_notification', 'read_message', 'is_active', 'is_admin', 'is_merchant', 'merchant_type', 'verification', 'verification_steps', 'univ_change_date', 'owner_deleted_at', 'subscribe'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    public function findForPassport($identifier) {
//        return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
//    }


    protected $dates = ['deleted_at'];

    /*
    Methods
    */


    public function merchant()
    {
        return $this->hasOne(MerchantDetail::class)->select('user_id', 'phone', 'slug', 'website', 'cover_image');
    }
    public function viewmessage(){
        return $this->belongsTo(Product::class,'user_id','id')->select('id','user_id');
    }
    // public function parentmarchat(){
    //    return $this->belongTo(User::class, 'user_id', 'id');
    // } 
    public function profileImage()
    {
        return $this->belongsTo(ProfileImage::class, 'profile_pic', 'id')
            ->select('user_id', 'id', 'file_name');
    }

    public function productExist()
    {
        return $this->hasOne(Product::class)->select('id', 'user_id');
    }

    public function notifications()
    {
        return $this->hasMany(Notification::class)->with('owner.profileImage', 'product', 'createdBy.profileImage')->orderBy('created_at', 'desc');
    }

    public function products()
    {
        return $this->hasMany(Product::class)->orderBy('created_at', 'desc');
    }

    public function productsWithBookedList()
    {
        return $this->hasMany(Product::class)->with('classShifts.shiftBook.bookedBy.profileImage')->orderBy('created_at', 'desc');
    }

    public function productInConversation()
    {
        $authId = $this->id;
        $productInConversation = collect(Message::whereNotIn('product_id', $this->messageProductsIds())->where(function ($query) use ($authId) {
            $query->where('from', $authId)->orWhere('to', $authId);
        })->orderBy("created_at")->pluck('product_id'));
        $productInConversation = $productInConversation->unique();

        $products = collect(Product::with('coverImage')->whereIn('id', $productInConversation)->select('id', 'name', 'slug')->get());
        foreach ($products as $key => $product) {
            $products[$key]["unreadThread"] = $product->msgUnreadThread();
            $products[$key]["lastMessageWith"] = $product->lastMessageWith();
        }
        return $products;
    }

    public function myProductInConversation()
    {
        $authId = $this->id;
        $productInConversation = collect(Message::whereIn('product_id', $this->messageProductsIds())->where(function ($query) use ($authId) {
            $query->where('from', $authId)->orWhere('to', $authId);
        })->orderBy("created_at")->pluck('product_id'));
        $productInConversation = $productInConversation->unique();

        $products = collect(Product::with('coverImage')->whereIn('id', $productInConversation)->select('id', 'name', 'slug')->get());
        foreach ($products as $key => $product) {
            $products[$key]["unreadThread"] = $product->msgUnreadThread();
            $products[$key]["lastMessageWith"] = $product->lastMessageWith();
        }
        return $products;
    }


//
//    public function sendPasswordResetNotification($token)
//    {
//        // Your your own implementation.
//        $this->notify(new ResetPasswordNotification($token));
//    }
//
//
//
//    public function university()
//    {
//        return $this->belongsTo(University::class,'university_id','id');
//    }
//
//    public function merchant(){
//        return $this->hasOne(MerchantDetail::class);
//    }
//
//    public function notifications()
//    {
//        return $this->belongsToMany(Notification::class)->withPivot('seen','id')->orderBy('created_at', 'desc');
//    }
//
//    public function outbox()
//    {
//        return $this->hasMany(Message::class,'from')->orderBy('created_at', 'desc');
//    }
//    public function inbox()
//    {
//        return $this->hasMany(Message::class,'to')->orderBy('created_at', 'desc');
//    }
//
//    public function commentReply(){
//        return $this->hasMany(ProductComment::class,'user_id');
//    }
//
//
//    public function unseenInbox()
//    {
//        return $this->hasMany(Message::class,'to')->orderBy('created_at', 'desc')->where('seen',0);
//    }
//
//    public function unseenNotifications()
//    {
//        return $this->belongsToMany(Notification::class)->wherePivot('seen', 0)->withPivot('seen','id')->orderBy('created_at', 'desc');
//    }
//
//    public function seenNotifications()
//    {
//        return $this->belongsToMany(Notification::class)->wherePivot('seen', 1)->withPivot('seen','id')->orderBy('created_at', 'desc');
//    }
//
//    public function wishList()
//    {
//        return $this->belongsToMany(Product::class,'wishlist_details');
//    }
//
//    public function socialAccounts()
//    {
//        return $this->hasMany('App\Model\SocialAccount','user_id','id');
//    }
//    public function token()
//    {
//        return $this->hasOne(UserRequest::class);
//    }
//
//    public function products()
//    {
//        return $this->hasMany(Product::class)->where('owner_deleted_at',null)->orderBy('created_at','desc');
//    }
//
//    public function application($productId){
//        $application = Applicant::where("user_id",$this->id)->where("product_id",$productId)->first();
//        return $application;
//    }
//
//    // Message product of owner product
    public function messageProductsIds()
    {
        // collect all the product id of this owner
        $productIds = $this->products()->pluck('id');
        // collect all the product which have unseen message from any user
        $messageActiveProductIds = collect(Message::whereIn('product_id', $productIds)->where('seen', 0)->orderBy('created_at', 'desc')->pluck('product_id'));
        // collect all the product which have seen message from any user
        $messageInActiveProductIds = collect(Message::whereIn('product_id', $productIds)->where('seen', 1)->orderBy('created_at', 'desc')->pluck('product_id'));
        $messageActiveProductIds = $messageActiveProductIds->unique();
        $messageInActiveProductIds = $messageInActiveProductIds->unique();
        $allProductIds = $messageActiveProductIds->merge($messageInActiveProductIds);
        // merge all the read and unread message id array
        return $allProductIds->unique();

    }
//
//    public function findProduct($id){
//        return Product::find($id);
//    }
//
//
//    public function threeProducts()
//    {
//        return $this->hasMany(Product::class)->where('sold_to_user_id',null)->inRandomOrder()->limit(10);
//    }
//    public function sixSoldProducts()
//    {
//        return $this->hasMany(Product::class)->where('sold_to_user_id','<>',null)->limit(6);
//    }
//    public function soldProducts()
//    {
//        return $this->hasMany(Product::class)->where('sold_to_user_id','<>',null);
//    }
//
//
////    public function rating()
////    {
////        return $this->hasMany(UserReview::class);
////    }
////
////    public function userReview($owner)
////    {
////        $review=UserReview::where('rated_by_user_id',$this->id)->where('user_id',$owner)->first();
////        return $review;
////    }
//
//    public function productToCart()
//    {
//        $productList=Message::whereNotIn('product_id',$this->products->pluck('id')->toArray())
//            ->where(function ($query) {
//                $query->where('from',$this->id)
//                    ->orWhere('to',$this->id);
//            })
//            ->pluck('product_id')
//            ->toArray();
//        //$user=User::whereIn('id',$message)->get();
//        return Product::whereIn('id',$productList)->get();
//    }
//
    public function msgUnreadThread()
    {
        $productListArray = array();
        foreach ($this->productToCart() as $productList) {
            array_push($productListArray, $productList->id);
        }

        $senderIds = Message::where('from', $this->id)->groupBy('to')->pluck('to')->toArray();
        $receiverIds = Message::where('to', $this->id)->groupBy('from')->pluck('from')->toArray();

        $connectedUserIds = array_merge($senderIds, $receiverIds);
        $connectedUserIds = array_unique($connectedUserIds);
        $messages = array();
        $unreadThread = 0;
        foreach (array_keys($productListArray) as $productListCounter) {

            //Receiver is auth user
            $messageHistory = array();
            $messageHistory['product'] = Product::find($productListArray[$productListCounter]);


            $send = Message::where('from', $messageHistory['product']->owner->id)->where('to', $this->id)->where('product_id', $productListArray[$productListCounter])->get();
            //Sender is auth user
            $receive = Message::where('to', $messageHistory['product']->owner->id)->where('from', $this->id)->where('product_id', $productListArray[$productListCounter])->get();

            $final = $receive->toBase()->merge($send)->sortBy('created_at');

            $existUnreadMessage = false;
            foreach ($final as $singleMessage) {
                if ($singleMessage->seen == 0 && $singleMessage->to == $this->id) {
                    $existUnreadMessage = true;
                }
            }
            if ($existUnreadMessage) $unreadThread++;

            $messageHistory['messages'] = $final;
            array_push($messages, $messageHistory);

        }


        // foreach ($messages as $message) {
        //  echo $message['Member']->first_name.' <br> ';
        //  foreach ($message['messages'] as $singleMessage) {
        //      echo $singleMessage->message . ' <br> ';
        //  }
        // }

        return $unreadThread;
    }
//
//    public function productInConversation(){
//        $authId=$this->id;
//        $productInConversation = collect(Message::whereNotIn('product_id',$this->messageProductsIds())->where(function ($query) use ($authId){
//            $query->where('from',$authId)->orWhere('to',$authId);
//        })->orderBy("created_at")->pluck('product_id'));
//        $productInConversation = $productInConversation->unique();
//        return Product::whereIn('id',$productInConversation)->get();
//    }


}
