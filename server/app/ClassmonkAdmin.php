<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ClassmonkAdmin extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $fillable = ['username', 'email', 'password', 'transaction_password', 'admin_level', 'profile_pic'];

    protected $table="admins";

    protected $guard = "admin";
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

//    public function findForPassport($identifier) {
//        return $this->orWhere('email', $identifier)->orWhere('username', $identifier)->first();
//    }

    protected $dates = ['deleted_at'];
}
