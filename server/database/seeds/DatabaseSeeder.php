<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CategoriesTableSeeder::class);
        $this->call(CityTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);

        $this->call(AbuseReportsTableSeeder::class);
        $this->call(AdminTableSeeder::class);



        $this->call(CityProductTableSeeder::class);
        $this->call(ClassPackageTableSeeder::class);
        $this->call(ClassShiftTableSeeder::class);
        $this->call(CommonTableSeeder::class);
        $this->call(MerchantTableSeeder::class);
        $this->call(MessagesTableSeeder::class);
        $this->call(PackageDateTableSeeder::class);
        $this->call(PagesTableSeeder::class);

        $this->call(ProfileImagesTableSeeder::class);
        $this->call(ShiftbooksTableSeeder::class);
        $this->call(SocialAccountTableSeeder::class);
        $this->call(TransactionTableSeeder::class);
        $this->call(WishlistDetailsTableSeeder::class);


    }
}