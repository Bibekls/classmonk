<?php

use App\Model\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $categoryList = [
        [
            "Computer",
            [
                "Vfx",
                "Office Package",
                "Graphic Packages",
                "Java Programing",
                "Full stack Development",
                "Database Administrator",
                "System Administrator"
            ]
        ],
        [
            "Cooking",
            [
                "Vegetarian",
                "Non-Veg",
                "Frying",
                "Bakery"
            ]
        ],
        [
            "Dancing",
            [
                "Zydeco Dancing",
                "Ballroom Dancing",
                "Nightclub Two Step",
                "Latin Dance",
                "Lindy Hop",
                "Rumba Dance"
            ]
        ],
        [
            "Driving",
            [
                "Motercycle",
                "Car",
                "Scoty",
                "Truck",
                "Bus",
                "Racing Bike"
            ]
        ],
        [
            "Guitar",
            [
                "Acoustic",
                "Electric",
                "Twelve-string",
                "Archtop",
                "Bass"
            ]
        ],
        [
            "Martial Arts",
            [
                "Kickboxing",
                "Karate",
                "Tae Kwon Do",
                "Kung Fu"
            ]
        ], [
            "Painting",
            [
                "Abstract Art",
                "Surrealism",
                "Conceptual Art",
                "Pop Art",
                "Photorealism",
                "Hyperrealism"
            ]
        ],
        [
            "Photography",
            [
                "Aerial",
                "Adventure, Action",
                "Animal, Pet",
                "Artistic",
                "Camera Phone",
                "Documentary",
                "Modeling",
                "Scientific"
            ]
        ],
        [
            "Yoga",
            [
                "Anusara",
                "Ashtanga",
                "Jivamukti",
                "Kripalu",
                "Kundalini",
                "Prenatal",
                "Restorative",
            ]
        ]
    ];

    protected function slugify($text)
    {
        $text = preg_split('/[^[:alnum:]]+/', $text);
        $finalText = "";
        foreach ($text as $value) {
            if ($finalText == "") $finalText = $value;
            else $finalText = $finalText . "-" . $value;
        }
        return strtolower($finalText)??str_random(8);
    }


    public function run()
    {
        Category::create([
            "parent_id" => "1",
            "name" => "root",
            "slug" => "root",
            "description" => "This is root row for pointing the parent",
            "status" => 0,
            "sort_order" => 0,
        ]);
        foreach ($this->categoryList as $category) {
            $parent = Category::create([
                "parent_id" => "1",
                "name" => $category[0],
                "slug" => $this->slugify($category[0]),
                "description" => "",
                "status" => 0,
                "sort_order" => 0,
            ]);
            foreach ($category[1] as $subCategory) {
                Category::create([
                    "parent_id" => $parent->id,
                    "name" => $subCategory,
                    "slug" => $this->slugify($subCategory),
                    "description" => "",
                    "status" => 0,
                    "sort_order" => 0,
                ]);
            }
        }
    }
}
