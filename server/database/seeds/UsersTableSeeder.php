<?php

use App\Model\ProfileImage;
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $counterRange = 697;

    public function getRandomImage()
    {
        $randomFile = rand(10, $this->counterRange);


    }

    public function incrementFilename($fileDirectory)
    {

        if (is_dir($fileDirectory)) {
            if ($dh = opendir($fileDirectory)) {
                $counter = 0;
                while (($file = readdir($dh)) !== false) {
                    // there is . and  .. link so skip it
                    if (!($file === "." || $file === "..")) {
                        print $file . ' => ' . $counter . PHP_EOL;
                        $counter++;
                        rename($fileDirectory . $file,
                            $fileDirectory . "/renamed/img (" . $counter . ")." . explode(".", $file)[1]);
                    }
                }
                closedir($dh);
            }
        }
    }

    public function run()
    {
        $this->userSeed();
        $this->merchantSeed();
    }

    private function userSeed()
    {
        //$this->incrementFilename(public_path() . '/image_sets/profile_pic/');

        $faker = Faker\Factory::create();

        if (!file_exists(public_path() . '/images/profile-pic/')) {
            mkdir(public_path() . '/images/profile-pic/', 0777, true);
        }

        // seed the user to user tables
        for ($i = 0; $i < env('DB_SEED_SCALE') * 1; $i++) {

            //check if image exists or not

            //check of jpg image
            $file_type = '';

            if (file_exists(public_path() . '/image_sets/profile_pic/img (' . ($i % $this->counterRange + 1) . ').jpg')) {
                $file_type = 'jpg';

            } else if (file_exists(public_path() . '/image_sets/profile_pic/img (' . ($i % $this->counterRange + 1) . ').jpeg')) {
                $file_type = 'jpeg';

            } else if (file_exists(public_path() . '/image_sets/profile_pic/img (' . ($i % $this->counterRange + 1) . ').png')) {
                $file_type = 'png';

            } else if (file_exists(public_path() . '/image_sets/profile_pic/img (' . ($i % $this->counterRange + 1) . ').gif')) {
                $file_type = 'gif';
            }

            $user = User::create([
                'username' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->name,
                'phone' => $faker->phoneNumber,
                'profile_pic' => $file_type,
                'first_name' => $faker->name,
                'last_name' => $faker->name,
                'city' => $faker->city,
                'is_active' => true,
                'is_merchant' => false,
                'merchant_type' => '1',
                'verification' => '1',
                'verification_steps' => '1'
            ]);

            $filename = uniqid() . '.' . $file_type;

            try {
                copy(public_path() . '/image_sets/profile_pic/img (' . ($i % $this->counterRange + 1) . ').' . $file_type,
                    public_path() . '/images/profile-pic/' . $filename . '.' . $file_type);
                print 'Transfer ' . $i . PHP_EOL;
            } catch (Exception $e) {
                print 'Error on ' . $i . PHP_EOL;
            }

            $image = ProfileImage::create([
                'user_id' => $user->id,
                'file_name' => $filename,
                'file_size' => 'n/a',
                'file_mime' => 'n/a',
                'file_path' => 'n/a',
            ]);

            $user->update(['profile_pic' => $image->id]);

            if (!file_exists(public_path() . '/images/profile-pic/thumbnails')) {
                mkdir(public_path() . '/images/profile-pic/thumbnails', 0777, true);
            }

            try {
                Image::make(public_path() . '/images/profile-pic/' . $filename . '.' . $file_type)
                    ->fit(64, 64)->save(public_path() . '/images/profile-pic/thumbnails/' . $filename . '.' . $file_type, 50);
            } catch (Exception $e) {
                print $e . PHP_EOL;
            }

            if (!file_exists(public_path() . '/images/profile-pic/thumbs')) {
                mkdir(public_path() . '/images/profile-pic/thumbs', 0777, true);
            }

            try {
                Image::make(public_path() . '/images/profile-pic/' . $filename . '.' . $file_type)
                    ->fit(128, 128)->save(public_path() . '/images/profile-pic/thumbs/' . $filename . '.' . $file_type, 50);
            } catch (Exception $e) {
                print $e . PHP_EOL;
            }


            //            $image = ProductImage::create([
            //                'product_id' => $product->id,
            //                'file_name' => $filename,
            //                'file_size' => $productImage->getClientSize(),
            //                'file_mime' => $productImage->getClientMimeType(),
            //                'file_path' => $this->imageServer . '/images/product-image/' . $filename,
            //                'is_cover_image' => ($count == 0) ? true : false,
            //            ]);
            //
            //
            //            if (!file_exists($this->imageServer . '/images/product-image/small')) {
            //                mkdir($this->imageServer . '/images/product-image/small', 0777, true);
            //            }
            //
            //            Image::make($this->imageServer . '/images/product-image/' . $filename)
            //                ->resize(800, null, function ($constraint) {
            //                    $constraint->aspectRatio();
            //                })->save($this->imageServer . '/images/product-image/small/' . $filename, 100);
            //

        }
    }

    private function merchantSeed(){
        //$this->incrementFilename(public_path() . '/image_sets/business_logo/');

        $faker = Faker\Factory::create();

        if (!file_exists(public_path() . '/images/profile-pic/')) {
            mkdir(public_path() . '/images/profile-pic/', 0777, true);
        }

        // seed the user to user tables
        for ($i = 0; $i < env('DB_SEED_SCALE') * 1; $i++) {


            //check if image exists or not

            //check of jpg image
            $file_type = '';

            if (file_exists(public_path() . '/image_sets/business_logo/img (' . ($i % $this->counterRange + 1) . ').jpg')) {
                $file_type = 'jpg';

            } else if (file_exists(public_path() . '/image_sets/business_logo/img (' . ($i % $this->counterRange + 1) . ').jpeg')) {
                $file_type = 'jpeg';

            } else if (file_exists(public_path() . '/image_sets/business_logo/img (' . ($i % $this->counterRange + 1) . ').png')) {
                $file_type = 'png';

            } else if (file_exists(public_path() . '/image_sets/business_logo/img (' . ($i % $this->counterRange + 1) . ').gif')) {
                $file_type = 'gif';
            }

            $user = User::create([
                'username' => $faker->name,
                'email' => $faker->email,
                'password' => $faker->name,
                'phone' => $faker->phoneNumber,
                'profile_pic' => $file_type,
                'first_name' => $faker->name,
                'last_name' => $faker->name,
                'city' => $faker->city,
                'is_active' => true,
                'is_merchant' => true,
                'merchant_type' => '1',
                'verification' => '1',
                'verification_steps' => '1'
            ]);

            $filename = uniqid() . '.' . $file_type;

            try {
                copy(public_path() . '/image_sets/business_logo/img (' . ($i % $this->counterRange + 1) . ').' . $file_type,
                    public_path() . '/images/profile-pic/' . $filename);
                print 'Transfer ' . $i . PHP_EOL;
            } catch (Exception $e) {
                print 'Error on ' . $i . PHP_EOL;
            }

            $image = ProfileImage::create([
                'user_id' => $user->id,
                'file_name' => $filename,
                'file_size' => 'n/a',
                'file_mime' => 'n/a',
                'file_path' => 'n/a',
            ]);

            $user->update(['profile_pic' => $image->id]);

            if (!file_exists(public_path() . '/images/profile-pic/thumbnails')) {
                mkdir(public_path() . '/images/profile-pic/thumbnails', 0777, true);
            }

            try {
                Image::make(public_path() . '/images/profile-pic/' . $filename)
                    ->fit(64, 64)->save(public_path() . '/images/profile-pic/thumbnails/' . $filename , 50);
            } catch (Exception $e) {
                print $e . PHP_EOL;
            }

            if (!file_exists(public_path() . '/images/profile-pic/thumbs')) {
                mkdir(public_path() . '/images/profile-pic/thumbs', 0777, true);
            }

            try {
                Image::make(public_path() . '/images/profile-pic/' . $filename )
                    ->fit(128, 128)->save(public_path() . '/images/profile-pic/thumbs/' . $filename , 50);
            } catch (Exception $e) {
                print $e . PHP_EOL;
            }


            //            $image = ProductImage::create([
            //                'product_id' => $product->id,
            //                'file_name' => $filename,
            //                'file_size' => $productImage->getClientSize(),
            //                'file_mime' => $productImage->getClientMimeType(),
            //                'file_path' => $this->imageServer . '/images/product-image/' . $filename,
            //                'is_cover_image' => ($count == 0) ? true : false,
            //            ]);
            //
            //
            //            if (!file_exists($this->imageServer . '/images/product-image/small')) {
            //                mkdir($this->imageServer . '/images/product-image/small', 0777, true);
            //            }
            //
            //            Image::make($this->imageServer . '/images/product-image/' . $filename)
            //                ->resize(800, null, function ($constraint) {
            //                    $constraint->aspectRatio();
            //                })->save($this->imageServer . '/images/product-image/small/' . $filename, 100);
            //

        }
    }

}

