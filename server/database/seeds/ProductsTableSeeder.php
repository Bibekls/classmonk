<?php

use App\Model\Category;
use App\Model\Product;
use App\User;
use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    protected function slugify($text)
    {
        $text = preg_split('/[^[:alnum:]]+/', $text);
        $finalText = "";
        foreach ($text as $value) {
            if ($finalText == "") $finalText = $value;
            else $finalText = $finalText . "-" . $value;
        }

        return strtolower($finalText)??str_random(8);
    }

    public function incrementFilename($fileDirectory)
    {

        if (is_dir($fileDirectory)) {
            if ($dh = opendir($fileDirectory)) {
                $counter = 0;
                while (($file = readdir($dh)) !== false) {
                    // there is . and  .. link so skip it
                    if (!($file === "." || $file === ".." || $file === "renamed")) {
                        print $file . ' => ' . $counter . PHP_EOL;
                        $counter++;
                        rename($fileDirectory . $file,
                            $fileDirectory . "/renamed/img (" . $counter . ")." . explode(".", $file)[1]);
                    }
                }
                closedir($dh);
            }
        }
    }

    private $category = [
        "computer",
        "cooking",
        "dancing",
        "driving",
        "guitar",
        "martial_arts",
        "painting",
        "photography",
        "yoga",
    ];

    public function prepareImage()
    {
        foreach ($this->category as $categoryName) {
            if (!file_exists(public_path() . '/image_sets/' . $categoryName . '/renamed')) {
                mkdir(public_path() . '/image_sets/' . $categoryName . '/renamed', 0777, true);
            }
            print "Preparing=>>>>>>>>>>>> " . $categoryName . PHP_EOL;
            $this->incrementFilename(public_path() . '/image_sets/' . $categoryName . '/');
        }
    }

    public function run()
    {
        //$this->prepareImage();

        $faker = Faker\Factory::create();
        $merchants = User::where('is_merchant', true)->get();
        $categories = Category::where('parent_id', 1)
            ->where('id', '<>', 1)->orderBy("slug")->get();
        $cities = \App\Model\City::all();

        if (!file_exists(public_path() . '/images/product-image/')) {
            mkdir(public_path() . '/images/product-image/', 0777, true);
        }


        //manually setting image count
        $imageCountOfCategory = [
            347,
            300,
            250,
            322,
            264,
            255,
            265,
            305,
            371
        ];
        for ($i = 0; $i < $categories->count(); $i++) {
            $categories[$i]["totalImage"] = $imageCountOfCategory[$i];
            print $categories[$i].PHP_EOL;
        }

        foreach ($merchants as $merchant) {
            $count = 0;
            foreach ($categories as $category) {

                // create image counter
                if (!$category["i"]) $category["i"] = 1;
                //

                if (rand(0, 2))
                    continue;

                foreach ($category->subCategory as $subCategory) {
                    if (rand(0, 1))
                        continue;

                    $count++;
                    $name = $faker->sentence($nbWords = 6, $variableNbWords = true);
                    $randomCityIndex = rand(0, $cities->count() - 4);

                    $productClass = Product::create([
                        "category_id" => $category->id,
                        "sub_category_id" => $subCategory->id,
                        "user_id" => $merchant->id,
                        "name" => $name,
                        "slug" => $this->slugify($name),
                        "final_price" => '',
                        "view_count" => rand(100, 1000),
                        "description" => $faker->paragraph($nbSentences = rand(10, 15),
                            $variableNbSentences = true),
                        "location" => $cities[$randomCityIndex]->name . ' ' . rand(1000, 5000),
                        "wsjtc" => $faker->realText($maxNbChars = rand(100, 200), $indexSize = rand(1, 2)),
                        "hlatc" => $faker->realText($maxNbChars = rand(100, 200), $indexSize = rand(1, 2)),
                        "amenities" => '',
                        "total_seat" => rand(5, 15),
                        "class_type" => null,
                        "is_available" => true,
                        "other_features" => '',
                        "available_from" => date("Y-m-d"),
                        "owner_deleted_at" => $count > 3 ? date("Y-m-d") : null,
                    ]);


                    \App\Model\CityProduct::create([
                        "product_id" => $productClass->id,
                        "city_id" => $cities[$randomCityIndex]->id,
                    ]);

                    if (rand(0, 5))
                        \App\Model\CityProduct::create([
                            "product_id" => $productClass->id,
                            "city_id" => $cities[$randomCityIndex + rand(1,3)]->id,
                        ]);


                    // create random image betn 3-8

                    for($classImageCounter = 0 ; $classImageCounter < rand(2,4) ; $classImageCounter++){

                        ////////////////////////////// Image section starat
                        //check if image exists or not

                        //check of jpg image

                        retry:

                        $file_type = '';

                        if (file_exists(public_path() . '/image_sets/'.$category->name.'/img (' . ($category["i"] % $category["totalImage"]) . ').jpg')) {
                            $file_type = 'jpg';


                        } else if (file_exists(public_path() . '/image_sets/'.$category->name.'/img (' . ($category["i"] % $category["totalImage"]) . ').jpeg')) {
                            $file_type = 'jpeg';


                        } else if (file_exists(public_path() . '/image_sets/'.$category->name.'/img (' . ($category["i"] % $category["totalImage"]) . ').png')) {
                            $file_type = 'png';


                        } else if (file_exists(public_path() . '/image_sets/'.$category->name.'/img (' . ($category["i"] % $category["totalImage"]) . ').gif')) {
                            $file_type = 'gif';
                        }


                        $filename = uniqid() . '.' . $file_type;

                        try {
                            copy(public_path() . '/image_sets/'.$category->name.'/img (' . ($category["i"] % $category["totalImage"]) . ').' . $file_type,
                                public_path() . '/images/product-image/' . $filename);
                            print 'Transfer ' . $category->name.' '.$category["i"] . PHP_EOL;

                        } catch (Exception $e) {
                            $category["i"] = $category["i"] + 1;
//                            print 'Error on ' . $category->name.' '.$category["i"] . PHP_EOL;
//                            print $e.PHP_EOL;
                            goto retry;
                        }

                        if (!file_exists(public_path() . '/images/product-image/small')) {
                            mkdir(public_path() . '/images/product-image/small', 0777, true);
                        }

                        try {
                            Image::make(public_path() . '/images/product-image/' . $filename)
                                ->resize(800, null, function ($constraint) {
                                    $constraint->aspectRatio();
                                })->save(public_path() . '/images/product-image/small/' . $filename , 100);

                        } catch (Exception $e) {
                            //print $e . PHP_EOL;
                        }

                        $image = \App\Model\ProductImage::create([
                            'product_id' => $productClass->id,
                            'file_name' => $filename,
                            'file_size' => 'n/a',
                            'file_mime' => 'n/a',
                            'file_path' => 'n/a',
                            'is_cover_image'=>$classImageCounter?false:true
                        ]);

                        //we have to increase category at last
                        $category["i"] = $category["i"] + 1;

                    }
                    print $productClass->name . PHP_EOL;
                }
            }
        }
    }
}
