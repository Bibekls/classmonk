<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelationConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        return 0;

        Schema::table('products', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('categories');
        });

        Schema::table('abuse_reports', function($table) {
            $table->foreign('abuse_report_type_id')->references('id')->on('abuse_report_types');
        });

        Schema::table('messages', function($table) {
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('from')->references('id')->on('users');
            $table->foreign('to')->references('id')->on('users');
        });

        Schema::table('wishlist_details', function($table) {
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::table('class_packages', function($table) {
            $table->foreign('product_id')->references('id')->on('products');
        });

        Schema::table('class_shifts', function($table) {
            $table->foreign('class_package_id')->references('id')->on('class_packages');
            $table->foreign('product_id')->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return 0;

        Schema::table('products', function($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['category_id']);
        });

        Schema::table('abuse_reports', function($table) {
            $table->dropForeign(['abuse_report_type_id']);
        });

        Schema::table('messages', function($table) {
            $table->dropForeign(['product_id']);
            $table->dropForeign(['from']);
            $table->dropForeign(['to']);
        });

        Schema::table('wishlist_details', function($table) {
            $table->dropForeign(['user_id']);
            $table->dropForeign(['product_id']);
        });

        Schema::table('class_packages', function($table) {
            $table->dropForeign(['product_id']);
        });

        Schema::table('class_shifts', function($table) {
            $table->dropForeign(['class_package_id']);
            $table->dropForeign(['product_id']);
        });
    }
}
