<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_images', function (Blueprint $table) {
            $table->increments('id');   

            $table->integer('product_id')->unsigned();
            $table->string('file_name',255);
            $table->string('file_size',255);
            $table->string('file_mime',255);
            $table->string('file_path',255);
            $table->boolean('is_cover_image')->default(false);

            $table->date('owner_deleted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_images');
    }
}
