<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');

            // type of the notification
            $table->integer('user_id')->unsigned();
            $table->integer('type')->unsigned();

            // notification type of comment reply

            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('comment_id')->unsigned()->nullable();

            // notification for shift booking

            $table->integer('shift_id')->unsigned()->nullable();
            $table->integer('no_of_seat')->unsigned()->nullable();
            $table->date('date')->nullable();
            $table->boolean('action')->nullable();

            // notification title and description

            $table->string('title',255);
            $table->text('description');

            //notification creator

            $table->integer('created_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
