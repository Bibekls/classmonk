<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AbuseReports extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abuse_reports', function (Blueprint $table) {
            $table->increments('id');   

            $table->integer('report_by_user')->unsigned();
            $table->integer('report_to_user')->unsigned();
            $table->integer('abuse_report_type_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->string('subject');
            $table->text('comment');

            $table->date('owner_deleted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abuse_reports');
    }
}
