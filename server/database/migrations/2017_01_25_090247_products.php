<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Products extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('products')){
            return 0;
        }
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('category_id')->unsigned();
            $table->integer('sub_category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name',128);
            $table->string('slug',150);
            $table->string('final_price',30)->nullable();
            $table->integer('view_count')->unsigned();
            $table->longText('description')->nullable();
            $table->string('location',100)->nullable();


            $table->string('wsjtc',512)->nullable();
            $table->string('hlatc',512)->nullable();
            $table->string('amenities')->nullable();
            $table->integer('total_seat')->nullable();
            $table->integer('class_type')->nullable();

            $table->integer('is_available')->unsigned();

            $table->longText('other_features')->nullable();

            $table->string('target_city',512)->nullable();

            $table->date('available_from')->nullable();
            $table->integer('varified_by')->unsigned();

            $table->date('owner_deleted_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
