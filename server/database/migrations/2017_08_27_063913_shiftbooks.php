<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Shiftbooks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shift_books', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('class_shift_id')->unsigned();
            $table->integer('no_of_seat')->unsigned();
            $table->integer('accept')->unsigned();
            $table->date('date')->nullable();
            $table->integer('booked_by')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shift_books');
    }
}
