<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Merchant extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('merchant')){
            return 0;
        }
        Schema::create('merchant', function (Blueprint $table) {
            $table->integer('user_id');
            $table->integer('merchant_type','2');
            $table->string('name',128)->nullable();
            $table->string('slug',150);
            $table->string('phone',15)->nullable();
            $table->string('website',50)->nullable();
            $table->string('address',128)->nullable();
            $table->longText('description')->nullable();
            $table->integer('cover_image')->nullable();
            $table->string('verified_steps')->nullable();
            $table->string('verified_data',100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('merchant');
    }
}
