<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('parent_id')->unsigned();
            $table->string('name', 50);
            $table->integer('is_class');
            $table->string('slug', 256);
            $table->text('description')->nullable();
            $table->integer('status')->unsigned();
            $table->integer('sort_order')->unsigned();
            $table->timestamps();
            $table->softDeletes();

            // $table->unique(["parent_id","name","slug"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
