<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('linked_card_id')->unsigned();
            $table->float('amount');
            $table->integer('user_id')->unsigned();
            $table->integer('class_shift_id')->unsigned();
            $table->text('log');
            $table->text('remark');
            $table->string('type', 15);
            $table->string('transaction_tag', 20);
            $table->string('authorization_number', 20);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
