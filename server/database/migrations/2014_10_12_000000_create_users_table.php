<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            return 0;
        }
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');


            $table->string('username')->nullable();
            $table->string('email');
            $table->string('password', 512)->nullable();

            $table->string('phone',30)->nullable();

            $table->string('profile_pic',512)->nullable();
            $table->string('first_name',50)->nullable();
            $table->string('last_name',50)->nullable();

            $table->string('city',50)->nullable();

            $table->boolean('read_notification')->default(false);
            $table->boolean('read_message')->default(false);

            $table->string('is_active',1)->nullable();

            $table->boolean('is_merchant')->default(false);
            $table->integer('merchant_type')->nullable();
            $table->integer('verification')->unsigned()->default(0);
            $table->string('verification_steps')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });


//        DB::unprepared('
//        CREATE TRIGGER tr_User_Default_Member_Role AFTER INSERT ON `users` FOR EACH ROW
//        BEGIN
//         INSERT INTO role_user (`role_id`, `user_id`, `created_at`, `updated_at`) VALUES (3, NEW.id, now(), null);
//        END
//        ');

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
        // DB::unprepared('DROP TRIGGER `tr_User_Default_Member_Role`');
    }
}
