<?php
Route::post('/userinfo','LoginController@login');
// ->middleware('client);
// Route::post('/userinfo','LoginController@UserInfo');

Route::post('/getinquiry','inquiryController@getInquiry');
Route::post('/getseeninquiry','inquiryController@getSeenInquiry');
Route::post('/view_message','inquiryController@viewMessage');
Route::post('/markas_spam','inquiryController@markAsSpam');
Route::post('mark_as_spam','inquiryController@MarkAsSpamData');


Route::post('/getnewclasses','newclassController@getNewClasses');
Route::get('/productidForapproved/{id}','newclassController@productIdapproved');
Route::get('/productidFordecliend/{id}','newclassController@productIddeclined');
Route::post('/declined_class','newclassController@declinedClass');
Route::post('/approved_class','newclassController@approvedClass');


Route::get('/get_instructor','instructorController@getIntructor');
Route::post('/verified_data','instructorController@verified_Data');
Route::get('declined_instructor/{id}','instructorController@declinedInstructor');
Route::get('suspend_instructor/{id}','instructorController@suspendInstructor');
Route::get('approved_instructor/{id}','instructorController@approvedInstructor');
Route::get('get_approved','instructorController@getApproved');
Route::get('get_declined','instructorController@getDeclined');
Route::get('get_suspend','instructorController@getSuspend');
Route::get('get_count','instructorController@getCount');


