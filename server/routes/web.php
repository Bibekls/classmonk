<?php

Route::post("connect", "WebController@connect");

Route::post("social-login", "WebController@socialLogin");

Route::post('/validate-email', "WebController@validateEmail");

Route::post('/member-register', 'Member\RegisterController@register');

Route::post('/instructor-register', 'Instructor\RegisterController@register');

Route::get('/get-city-list', 'WebController@getCityList');
Route::get('/get-category-list', 'WebController@getCategoryList');
Route::get('/get-amenities-list', 'WebController@getAmenitiesList');
Route::get('/product/{city}/{category}/{slug}', 'WebController@getProduct');
Route::post('/city-product', 'WebController@cityProduct');

Route::get('/check-authentication', 'WebController@checkAuthentication');

Auth::routes();

Route::get('bill', function (\Illuminate\Http\Request $request) {

    return view('welcome')->with('request', $request);
});

Route::post('receipt', function ($response) {
    Log::info();
});
Route::get('curl', function () {
    // Step 1
    $cSession = curl_init();
// Step 2
    curl_setopt($cSession, CURLOPT_URL, "https://demo.versapay.com/api/funds");
    curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cSession, CURLOPT_HEADER, false);
    curl_setopt($cSession, CURLOPT_USERPWD, 'L1kmqBmzADwQsz_w31wz:cADDsth723naHPPwqpBz');

    curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, 'GET');

// Step 3
    $result = curl_exec($cSession);
// Step 4
    curl_close($cSession);
// Step 5
    echo $result;

});


Route::get('exact-api', function () {
    // Step 1
    $cSession = curl_init();
// Step 2
    curl_setopt($cSession, CURLOPT_URL, "https://api.demo.e-xact.com/transaction");
    curl_setopt($cSession, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($cSession, CURLOPT_HEADER, false);
//    curl_setopt($cSession, CURLOPT_USERPWD,'AE6944-01:7bD7XWnl');

    curl_setopt($cSession, CURLOPT_HTTPHEADER, array('Content-Type: application/xml; charset=UTF-8', 'Accept: application/xml'));

    curl_setopt($cSession, CURLOPT_POSTFIELDS, '<?xml version="1.0" encoding="UTF-8"?>
<Transaction>
  <ExactID>AE6944-01</ExactID>
  <Password>7bD7XWnl</Password>
  
  <Transaction_Type>34</Transaction_Type>
  
  <Transaction_Tag>983932638</Transaction_Tag>
  <Authorization_Num>ET166945</Authorization_Num>    
  <DollarAmount>0.05</DollarAmount>   
    
</Transaction>');
    curl_setopt($cSession, CURLOPT_CUSTOMREQUEST, 'POST');

// Step 3
    $result = curl_exec($cSession);
// Step 4
    curl_close($cSession);
// Step 5
    echo '<pre>' . $result . '</pre>';

});


//983931120
//ET156227
//<Transaction_Tag>901975484</Transaction_Tag>
//<Authorization_Num>ET4653</Authorization_Num>
//<DollarAmount>15.75</DollarAmount>
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
