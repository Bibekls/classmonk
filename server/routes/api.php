<?php

use Illuminate\Support\Facades\Log;

Route::post("connect","ApiController@connect");

Route::post('/validate-email',"ApiController@validateEmail");

Route::post('/register','Member\RegisterController@register');

Route::post('/instructor-register','Instructor\RegisterController@register');

Route::get('/get-city-list','ApiController@getCityList');
Route::get('/get-category-list','ApiController@getCategoryList');
Route::get('/get-amenities-list','ApiController@getAmenitiesList');
Route::post('/product','ApiController@getProduct');
Route::post('/city-product','ApiController@cityProduct');

Route::get('/bill',function (\Illuminate\Http\Request $request){

    return view('welcome')->with('request',$request);
});


Route::post('receipt',function (\Illuminate\Http\Request $request){
    Log::info($request);
});


Route::middleware('auth:api')->get('/check-authentication','ApiController@checkAuthentication');
