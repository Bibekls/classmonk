<?php
// ADD CITY
Route::post("postcity","addCityController@postCity");
Route::get("getcity","addCityController@getCity");
Route::post("delete_city","addCityController@deleteCity");
Route::get("getdeletedcity","addCityController@getdeletedCity");
Route::get("reuse_city/{id}","addCityController@reuseCity");


// CATEGORY
Route::post("postcategory","categoryController@postCategory");
Route::get("parentcategory","categoryController@parentCategory");
Route::get("getcategorybysearch/{id}","categoryController@getcategoryBysearch");
Route::get("getpublishedcategory","categoryController@getpublishedCategory");
Route::get("getunpublishedcategory","categoryController@getUnPublishedCategory");
Route::get("unpublishcategory/{id}","categoryController@unPublishCategory");
Route::get("publishcategory/{id}","categoryController@PublishCategory");

// ADMIN MENUS
Route::get("getmenus","adminMenusController@getMenus");
Route::post("admin_user","adminMenusController@AdminUser");
Route::get("get_active_admin","adminMenusController@getactiveAdmin");
Route::get("editadmin/{id}","adminMenusController@editAdmin");
Route::get("make_Inactive_admin/{id}","adminMenusController@Make_Inactive_Admin");
Route::get("get_Inactive_admin","adminMenusController@get_Inactive_Admin");
Route::get("active_admin/{id}","adminMenusController@Active_Admin");

