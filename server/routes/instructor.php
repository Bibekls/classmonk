<?php

Route::resource('product', 'ProductController');
Route::get('dashboard/get-booked-shift','DashboardController@getBookedShift');
Route::post('/accept-booking-notification','NotificationController@acceptBookingNotification');
Route::post('/decline-booking-notification','NotificationController@declineBookingNotification');

Route::post('/get-messages','MessageController@loadMessage');
Route::post('/instructor-info','InstructorController@getInfo');
