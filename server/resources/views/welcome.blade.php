<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Styles -->

</head>
<body>

<?php
$x_amount = 0.01;
$x_fp_sequence = 123456;
$x_fp_timestamp = time();
$secretKey = "XgElzvPycjvYMS79dYjc";
$x_currency_code = "";
$x_login = "HCO-CLASS-910";

?>


<h2>Modal Example</h2>
<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">


                <div style="height: 280px ;overflow-x: hidden">
                    <iframe name="hidden_iframe" scrolling="yes" style="width:750px;height: 500px;margin-top: -230px" frameBorder="0"></iframe>
                    {{--<iframe style="margin-top:-100px;margin-left:-100px" height="350px" scrolling="no"--}}
                    {{--src="http://my/page/i/want/to/show/part/of/here/" width="500px"></iframe>--}}
                </div>


                <form id="paymentform" action="https://rpm.demo.e-xact.com/payment" method="post" target="hidden_iframe">
                    <input name="x_login" value="{{$x_login}}" type="hidden">
                    <input name="x_amount" value="{{$x_amount}}" type="hidden">
                    <input name="x_fp_sequence" value="{{$x_fp_sequence}}" type="hidden">
                    <input name="x_fp_timestamp" value="{{$x_fp_timestamp}}" type="hidden">
                    <input name="x_fp_hash"
                           value="<?php echo hash_hmac('md5', "{$x_login}^{$x_fp_sequence}^{$x_fp_timestamp}^{$x_amount}^{$x_currency_code}", $secretKey) ?>"
                           type="hidden">
                    <input name="x_show_form" value="PAYMENT_FORM" type="hidden">
                    <input name="x_relay_response" value="TRUE" type="hidden">
                    <input name="x_type" value="AUTH_TOKEN" type="hidden">

                    <input name="x_relay_url" value="http://server.bookmyseat.ca/api/relay-response" type="hidden">
                    {{--<input value="Checkout with E-­xact.com" type="submit">--}}


                    {{--<!-- Fields specific to Recurring -->--}}
                    {{--<input type="hidden" name="x_recurring_billing_amount" value="19.95" />--}}
                    {{--<input type="hidden" name="x_type" value="AUTH_ONLY" />--}}
                    {{--<input type="hidden" name="x_recurring_billing_id" value="MB-CLASS-151-403" />--}}
                    {{--<input type="hidden" name="x_recurring_billing" value="TRUE" />--}}
                    {{--<input type="hidden" name="x_recurring_billing_start_date" value="2017-11-11" />--}}
                    {{--<input type="hidden" name="x_recurring_billing_end_date" value="2018-05-11" />--}}


                </form>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>

    </div>
</div>


</body>
</html>

<script>
    $(document).ready(function () {
        $('#paymentform').submit();
    })
</script>
