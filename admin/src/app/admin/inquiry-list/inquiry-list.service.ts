import { Injectable } from '@angular/core';
import {Http, Response ,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
@Injectable()
export class InquiryListService {

  constructor(private http:Http) { }
  getInquiryList(data):Observable<any>{
      let headers = new Headers({'Content-Type': 'application/json'});
      let options = new RequestOptions({headers: headers, withCredentials: true});
      return this.http.post("http://classmonkserver.com/admin100/getinquiry",data,options)
             .map(this.extractData)
             .catch(this.handleError);
      }
      private extractData(res: Response) {
             let response = res.json();
             return response || {};
      }
      private handleError(error: Response | any) {
            return Observable.throw(error);
  }
  getSeenInquiry(data){
    let headers = new Headers({'Content-Type': 'application/json'});
      let options = new RequestOptions({headers: headers,withCredentials: true});
       return this.http.post("http://classmonkserver.com/admin100/getseeninquiry",data,options)
      .map(this.extractData)
      .catch(this.handleError); 
  }
  viewMenus(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.post("http://classmonkserver.com/admin100/view_message",data,options)
    .map(this.extractData)
    .catch(this.handleError); 
  }
  markasSpam(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.post("http://classmonkserver.com/admin100/markas_spam",data,options)
    .map(this.extractData)
    .catch(this.handleError);
  }
  MarkAspamData(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.post("http://classmonkserver.com/admin100/mark_as_spam",data,options)
    .map(this.extractData)
    .catch(this.handleError);
  }
}
