import { TestBed, inject } from '@angular/core/testing';

import { InquiryListService } from './inquiry-list.service';

describe('InquiryListService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InquiryListService]
    });
  });

  it('should be created', inject([InquiryListService], (service: InquiryListService) => {
    expect(service).toBeTruthy();
  }));
});
