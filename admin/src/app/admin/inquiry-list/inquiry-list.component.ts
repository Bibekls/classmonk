import { Component, OnInit } from '@angular/core';
import { InquiryListService } from './inquiry-list.service';
declare var jQuery: any;
declare var $;
@Component({
  selector: 'app-inquiry-list',
  templateUrl: './inquiry-list.component.html',
  styleUrls: ['./inquiry-list.component.css']
})
export class InquiryListComponent implements OnInit {
  public seen=false;
  public unseen=true;
  public markasSpamstatus=false;
  public inquiries=[];
  public SeenInquiryData=[];
  public messages=[];
  public sender;
  public reciver;
  public Email;
  public Name;
  public Phone;
  public student;
  public no_of_inquiry=0;
  public no_of_unseeninquiry=0;
  public lastInquiry=0;
  public pages=[];
  public seenpages=[];
  public lastSeeninquiry=0;
  public markasaSpamParam={};
  public markasSpamdata=[];
  public markpages=[];
  public lastmarkpage=0;
  constructor(private inquiryService:InquiryListService) { }
  ngOnInit() {
    this.InquiryLists(); 
    // this.seenInquiry();
  }
  showModel(){
    jQuery("#modal-info").modal("show");
  }
  InquiryLists(){
    let Param={}
    Param['page']=1; 
    this.inquiryService.getInquiryList(Param).subscribe(
      (response)=>{
         console.log(response);
         console.log(response.inquiry.data)
         for(let x in response.inquiry.data){
          this.inquiries.push(response.inquiry.data[x]);
         }
         console.log(this.inquiries)
          for(let x in this.inquiries){
            this.no_of_inquiry=parseInt(x)+1;
          }
          Param['page']=response.inquiry.current_page;
          this.lastInquiry=response.inquiry.current_page;
          this.pages.push(Param)
        }
     )
  }
  Previous(){
    let Param={}
    this.lastInquiry=this.lastInquiry-1;
    Param['page']=this.lastInquiry;
    this.inquiryService.getInquiryList(Param).subscribe(
  (response)=>{
    console.log(response);
    this.inquiries=[];
    console.log(response.inquiry.data)
    for(let x in response.inquiry.data){
       this.inquiries.push(response.inquiry.data[x]);
    }
    console.log(this.inquiries)
    this.pages.splice(-1);
  }
)
  }
  Next(){
     let Param={};
     this.lastInquiry=this.lastInquiry+1;
     Param['page']=this.lastInquiry;
     console.log(Param)
     this.inquiryService.getInquiryList(Param).subscribe(
        (response)=>{
           console.log(response);
           this.inquiries=[];
           console.log(response.inquiry.data)
           for(let x in response.inquiry.data){
              this.inquiries.push(response.inquiry.data[x]);
           }
           console.log(this.inquiries)
           console.log(response.inquiry.current_page)
           Param['page']=response.inquiry.current_page+1;
           if(this.pages.length < 4){
               this.pages.push(Param);
           }
         }
      )
   }
  middlePages(index){
    let page=this.pages[index].page;
    console.log(this.pages)
    console.log(page)
    let Param={}
    Param['page']=page;
    this.inquiryService.getInquiryList(Param).subscribe(
      (response)=>{
         this.inquiries=[];
         console.log(response.inquiry.data)
         for(let x in response.inquiry.data){
            this.inquiries.push(response.inquiry.data[x]);
         }
       }
    )

    console.log(index)
  }
  UnseenInquiry(){
    this.seen=false;
    this.unseen=true;
    this.markasSpamstatus=false;
    this.pages=[];
    this.inquiries=[]; 
    this.InquiryLists();
  }
  seenInquiry(){
    let Param={}
    this.seen=true;
    this.unseen=false;
    this.markasSpamstatus=false;
    this.lastSeeninquiry=1;
    Param['page']=this.lastSeeninquiry;
    this.inquiryService.getSeenInquiry(Param).subscribe(
      (response)=>{
        console.log(response.seeninquiry);
        this.SeenInquiryData=[];
        this.seenpages=[];
        for(let x in response.seeninquiry.data){
          this.SeenInquiryData.push(response.seeninquiry.data[x]);
        }
        Param['page']=response.seeninquiry.current_page+1;
        this.lastSeeninquiry=response.seeninquiry.current_page;
        this.seenpages.push(Param)
        for(let x in this.SeenInquiryData){
          this.no_of_unseeninquiry=parseInt(x)+1;
         }
       }
     )
   }
   seenviewMessage(index){
    let Param={}
    Param['senderId']=this.SeenInquiryData[index].sender_id;
    Param['reciverId']=this.SeenInquiryData[index].reciver_id;
    Param['productId']=this.SeenInquiryData[index].product_id;
    this.sender=this.SeenInquiryData[index].sender_id;
    this.reciver=this.SeenInquiryData[index].reciver_id;
     this.inquiryService.viewMenus(Param).subscribe(
       (response)=>{
           console.log(response)
           this.messages=response.viewmessage;
           this.Email=this.messages[0].email;
           this.Phone=this.messages[0].phone;
           this.Name=this.messages[0].name;
           this.student=this.messages[0].student
           jQuery("#requestviewmore").modal("show");
         }
       )
   }
   viewMessage(index){
    let Param={}
    Param['senderId']=this.inquiries[index].sender_id;
    Param['reciverId']=this.inquiries[index].reciver_id;
    Param['productId']=this.inquiries[index].product_id;
    this.sender=this.inquiries[index].sender_id;
    this.reciver=this.inquiries[index].reciver_id;
     this.inquiryService.viewMenus(Param).subscribe(
       (response)=>{
           this.messages=response.viewmessage;
           let flag=true;
          //  for(let x in this.messages){
          //     if(this.messages[x].from == this.sender){
          //          if(flag == true){
          //              this.messages[x].is_value=1;
          //              flag=false;
          //            }else{
          //             this.messages[x].is_value=0;
          //            }
          //         }
          //     }
          //  for(let y in this.messages){
          //       if(this.messages[y].to == this.sender){
          //         if(flag == true){
          //             this.messages[y].is_value=0;
          //             flag=false;
          //         }else{
          //           this.messages[y].is_value=1;
          //         }
          //       }  
          //  }
           console.log(this.messages)
           this.Email=this.messages[0].email;
           this.Phone=this.messages[0].phone;
           this.Name=this.messages[0].name;
           this.student=this.messages[0].student;
           jQuery("#requestviewmore").modal("show");
         }
       )
   } 
seenPrevious(){
  let Param={}
  if(this.lastSeeninquiry > 1){
    this.lastSeeninquiry=this.lastSeeninquiry-1;
    Param['page']=this.lastSeeninquiry;
    this.inquiryService.getSeenInquiry(Param).subscribe(
      (response)=>{
        console.log(response.seeninquiry);
        for(let x in response.seeninquiry.data){
          this.SeenInquiryData.push(response.seeninquiry.data[x]);
        }
        this.seenpages.splice(-1)
       }
     )
   }
  }
  seenNext(){
  let Param={}
  this.lastSeeninquiry=this.lastSeeninquiry+1;
  console.log(this.lastSeeninquiry)
  Param['page']=this.lastSeeninquiry;
  this.inquiryService.getSeenInquiry(Param).subscribe(
    (response)=>{
      console.log(response.seeninquiry);
      this.SeenInquiryData=[];
      for(let x in response.seeninquiry.data){
        this.SeenInquiryData.push(response.seeninquiry.data[x]);
      }
      Param['page']=response.seeninquiry.current_page+1;
       if(this.seenpages.length < 4){
        this.seenpages.push(Param)
       }
      for(let x in this.SeenInquiryData){
        this.no_of_unseeninquiry=parseInt(x)+1;
       }
     }
   )
  }
seenmiddlePage(index){
  let Param={}
  let page=this.seenpages[index].page
  Param['page']=page;
  this.inquiryService.getSeenInquiry(Param).subscribe(
    (response)=>{
      console.log(response.seeninquiry);
     for(let x in response.seeninquiry.data){
        this.SeenInquiryData.push(response.seeninquiry.data[x]);
      } 

     }
   )
 }  
 markAsaSpam(index){
      console.log(index);
      this.markasaSpamParam={}
      this.markasaSpamParam['senderId']=this.SeenInquiryData[index].sender_id;
      this.markasaSpamParam['reciverId']=this.SeenInquiryData[index].reciver_id;
      this.markasaSpamParam['productId']=this.SeenInquiryData[index].product_id;
      console.log(this.markasaSpamParam)
      jQuery("#markasSpam-modal-info").modal("show");
 }  
 markasaSpamModel(){
      jQuery("#markasSpam-modal-info").modal("hide");
      this.inquiryService.markasSpam(this.markasaSpamParam).subscribe(
        (response)=>{
                console.log(response)
          }
       )
    }
markAsSpam(){
  this.seen=false;
  this.unseen=false;
  this.markasSpamstatus=true;
  let Param={};
  Param['page']=1;
  this.inquiryService.MarkAspamData(Param).subscribe(
    (response)=>{
      this.markasSpamdata=[]
          console.log(response)
          for(let x in response.mark.data){
             this.markasSpamdata.push(response.mark.data[x]);
          }
          this.lastmarkpage=response.mark.current_page;
          Param['page']=this.lastmarkpage;
          this.markpages.push(Param)
        console.log(this.markasSpamdata)
      }
    )
  }
markPrevious(){
  this.lastmarkpage=this.lastmarkpage-1;
  let Param={};
  Param['page']=this.lastmarkpage;
  this.inquiryService.MarkAspamData(Param).subscribe(
    (response)=>{
      this.markasSpamdata=[]
          console.log(response)
          for(let x in response.mark.data){
             this.markasSpamdata.push(response.mark.data[x]);
          }
          this.lastmarkpage=response.mark.current_page;
          if(this.markpages.length < 1){
            this.markpages.splice(-1)
          }
        console.log(this.markasSpamdata)
      }
    )
} 
markmiddlePages(index){
  let page=this.markpages[index].page;
  let Param={};
  Param['page']=page;
  this.inquiryService.MarkAspamData(Param).subscribe(
    (response)=>{
      this.markasSpamdata=[]
          console.log(response)
          for(let x in response.mark.data){
             this.markasSpamdata.push(response.mark.data[x]);
          }
        console.log(this.markasSpamdata)
      }
    )

}
markNext(){
   this.lastmarkpage=this.lastmarkpage+1;
   let Param={}
   if(this.lastmarkpage > 1){
   Param['page']=this.lastmarkpage;
   this.inquiryService.MarkAspamData(Param).subscribe(
    (response)=>{
      this.markasSpamdata=[]
          console.log(response)
          for(let x in response.mark.data){
             this.markasSpamdata.push(response.mark.data[x]);
          }
          this.lastmarkpage=response.mark.current_page;
          Param['page']=this.lastmarkpage;
          if(this.markpages.length < 4){
            this.markpages.push(Param)
          }
        console.log(this.markasSpamdata)
      }
    )
   }
 }
}
