import { Component, OnInit } from '@angular/core';
import { ApproveInstructorService } from './approve-instructor.service';
import { FormBuilder, FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
declare var jQuery: any;
declare var $;
import * as sweetalert from 'sweetalert';

@Component({
  selector: 'app-approve-instructor',
  templateUrl: './approve-instructor.component.html',
  styleUrls: ['./approve-instructor.component.css']
})
export class ApproveInstructorComponent implements OnInit {
public instructorData=[];
public approvedInstructor: FormGroup;
public varificationArray=[];
public all=true;
public is_approved=false;
public is_declined=false;
public is_suspend=false;
public approvedLists=[];
public declinedLists=[];
public suspendLists=[];
public approvedform:FormGroup;
public declinedform:FormGroup;
public suspendform:FormGroup;
public suspendArray=[];
public approvedAyyar=[];
public declinedArray=[];
public number_of_instructor=0;
public number_of_approved=0;
public number_of_declined=0;
public number_of_suspend=0;
public New_title;
public New_body;
public New_status;
public IdForapproved;
public indexforapproved;
public declind_id;
public declinedindex;
public suspend_id;
public suspendindex;
public global_id;
public global_index;
constructor(private instructorService:ApproveInstructorService,private fb:FormBuilder) { }

  ngOnInit() {
      this.instructorService.getInstructorApproved().subscribe(
        (response)=>{
            console.log(response);
            this.instructorData=response.instructor;
            console.log(this.instructorData);
            for(let x in this.instructorData){
               this.number_of_instructor=parseInt(x)+1;
                console.log('menus data',this.instructorData[x].verified_data.email)
                this.varificationArray.push(this.fb.group({
                  email: this.instructorData[x].verified_data.email,
                  phonenumber:this.instructorData[x].verified_data.phonenumber,
                  website: this.instructorData[x].verified_data.website,
                  socialmedial:this.instructorData[x].verified_data.socialmedial,
                  id:this.instructorData[x].user_id,
                  cover_image:this.instructorData[x].cover_image,
              }));
            }
          this.approvedInstructor = this.fb.group({
               varificationLists:this.fb.array(this.varificationArray)
          });
          $(function() {
            $('#newtable').DataTable();
         }); 
         } 
    )
    this.instructorService.getCount().subscribe(
      (response)=>{
        console.log(response);
        this.number_of_approved=response.approved;
        this.number_of_declined=response.declined;
        this.number_of_suspend=response.suspend;
      }
    )
  }
  Approved(index){
    console.log(index)
    jQuery("#modal-info").modal("show");
    this.New_body="Do You Really want to Approved?"
    this.New_status=1;
    this.New_title="Approved Instructor"
    this.IdForapproved= this.instructorData[index].user_id;
    this.indexforapproved=index
  }
  approvedModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.approvedInstructor(this.IdForapproved).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.instructorData.splice( this.indexforapproved,1);
              this.number_of_instructor=this.number_of_instructor-1;
              this.number_of_approved=this.number_of_approved+1;
              swal("Good job!", "Sucessfully Approved!", "success");
            }
        },(error)=>{
               swal("Opp...!", "Unable to  Approved!", "error");
        }
     )
  }
  ApprovedChange(index){
    console.log(index)
    let verified_data;
    console.log('I am in')
    console.log(this.approvedInstructor.value)
    let data=this.approvedInstructor.value;
    for(let x in data.varificationLists){
      if(this.instructorData[index].user_id == data.varificationLists[x].id){
           verified_data=data.varificationLists[x];
           console.log(verified_data)
           break;
        }
     }
     this.instructorService.verifiedData(verified_data).subscribe(
       (response)=>{
         console.log(response)
         if(response){

         }
       }
     )
   }
 
   declinedInstructor(index){
    console.log(index)
     this.New_status=2;
     this.New_title="Declined Instructor"
     this.New_body="Do You Really want to Declined?"
     jQuery("#modal-info").modal("show");
     this.declind_id= this.instructorData[index].user_id;
     this.declinedindex=index;
   }
   declinedModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.declinedInstructor(this.declind_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.instructorData.splice(this.declinedindex,1);
              this.number_of_instructor=this.number_of_instructor-1;
              this.number_of_declined=this.number_of_declined+1;
              swal("Good job!", "Sucessfully Declined!", "success");
            }
        },(error)=>{
              swal("Opp...!", "Unable to  Declined!", "error");
        }
     )
   }
   suspendInstructor(index){
     console.log(index)
     jQuery("#modal-info").modal("show");
     this.New_status=3;
     this.New_title="Suspend Instructor"
     this.New_body="Do You Really want to Suspend?"
     this.suspend_id= this.instructorData[index].user_id;
     this.suspendindex=index;
  }
  suspendModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.suspendInstructor(this.suspend_id).subscribe(
      (response)=>{
        if(response){
          console.log(response)
          this.instructorData.splice(this.suspendindex,1);
          this.number_of_instructor=this.number_of_instructor-1;
          this.number_of_suspend=this.number_of_suspend+1;
          swal("Good job!", "Sucessfully Suspend!", "success");
        }
      },(error)=>{
         swal("Opp...!", "Unable to Suspend!", "error");
      })
  }
  All(){
    $(function() {
      $('#newtable').DataTable();
     }); 
    this.all=true;
    this.is_approved=false;
    this.is_declined=false;
    this.is_suspend=false;
  }
  AllApproved(){
    console.log('i am All Approved')
    this.all=false;
    this.is_approved=true;
    this.is_declined=false;
    this.is_suspend=false;
    this.instructorService.getApproved().subscribe(
      (response)=>{
          console.log(response)
          this.approvedLists=response.approved;
          for(let x in this.approvedLists){
            console.log('menus data',this.approvedLists[x].verified_data.email)
            this.approvedAyyar.push(this.fb.group({
              email: [{disabled:true,value:this.approvedLists[x].verified_data.email}],
              phonenumber:[{disabled:true,value:this.approvedLists[x].verified_data.phonenumber}],
              website: [{disabled:true,value:this.approvedLists[x].verified_data.website}],
              socialmedial:[{disabled:true,value:this.approvedLists[x].verified_data.socialmedial}],
              id:[{disabled:true,value:this.approvedLists[x].user_id}],
              cover_image:[{disabled:true,value:this.approvedLists[x].cover_image}],
          }));
        }
        // disabled: true
        this.approvedform= this.fb.group({
          approved_List:this.fb.array(this.approvedAyyar)
         });
          $(function() {
            $('#approvedTable').DataTable();
            }); 
      })
  }
  IsapprovedChange(index){
    console.log(index)
    let verified_data;
    console.log('I am in')
    console.log(this.approvedform.value)
    let data=this.approvedform.value;
    for(let x in data.approved_List){
      if(this.approvedLists[index].user_id == data.approved_List[x].id){
           verified_data=data.approved_List[x];
           console.log(verified_data)
           break;
        }
     }
     this.instructorService.verifiedData(verified_data).subscribe(
       (response)=>{
         console.log('i ma response Is approved changed',response)
       }
     )
  }
  declinedInstructorFormApprovedLists(index){
    jQuery("#modal-info").modal("show");
    this.global_id = this.approvedLists[index].user_id;
    this.global_index=index;
    this.New_body="Do You Really Want to declined?"
    this.New_title="Declined Instructor";
    this.New_status=4;
  }
  declinedformApprovedModel(){
     jQuery("#modal-info").modal("hide");
     this.instructorService.declinedInstructor(this.global_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.approvedLists.splice(this.global_index,1)
              this.number_of_approved=this.number_of_approved-1;
              this.number_of_declined=this.number_of_declined+1;
              swal("Good job!", "Sucessfully Declined!", "success");
            }
        },(error)=>{
          swal("Opp...!", "Unable to Declined!", "error");
        } 
     )
  }
  suspendInstructorFormApprovedLists(index){
    jQuery("#modal-info").modal("show");
    this.global_id = this.approvedLists[index].user_id;
    this.global_index=index;
    this.New_body="Do You Really Want to Suspend?"
    this.New_title="Suspend Instructor";
    this.New_status=5;
  }
  suspendformApprovedModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.suspendInstructor(this.global_id ).subscribe(
      (response)=>{
        if(response){
             console.log(response)
             this.approvedLists.splice(this.global_index,1)
             this.number_of_approved=this.number_of_approved-1;
             this.number_of_suspend=this.number_of_suspend+1;
             swal("Good job!", "Sucessfully Suspend!", "success");
           }
       },(error)=>{
         swal("Opp...!", "Unable to  Suspend!", "error");
       })   
  }
  AllDeclined(){
    console.log('i am All Declined')
    this.all=false;
    this.is_approved=false;
    this.is_declined=true;
    this.is_suspend=false;
    this.instructorService.getDeclined().subscribe(
      (response)=>{
            console.log(response);
            this.declinedLists=response.declined;
            for(let x in this.declinedLists){
              console.log('menus data',this.declinedLists[x].verified_data.email)
              this.declinedArray.push(this.fb.group({
                email: this.declinedLists[x].verified_data.email,
                phonenumber:this.declinedLists[x].verified_data.phonenumber,
                website: this.declinedLists[x].verified_data.website,
                socialmedial:this.declinedLists[x].verified_data.socialmedial,
                id:this.declinedLists[x].user_id,
                cover_image:this.declinedLists[x].cover_image,
            }));
          }
          this.declinedform= this.fb.group({
            declined_List:this.fb.array(this.declinedArray)
           });
            $(function() {
                 $('#declinedTable').DataTable();
              }); 
      })
  }
  ApprovedFormdeclined(index){
    jQuery("#modal-info").modal("show");
    this.global_id =this.declinedLists[index].user_id;
    this.global_index=index;
    this.New_body="Do You Really Want to Approved?"
    this.New_title="Approved Instructor";
    this.New_status=6;  
  }
  approvedfromdeclinedModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.approvedInstructor(this.global_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.declinedLists.splice(this.global_index,1)
              this.number_of_declined=this.number_of_declined-1;
              this.number_of_approved=this.number_of_approved+1;
              swal("Good job!", "Sucessfully Approved!", "success");
            }
        },(error)=>{
          swal("Opp...!", "Unable to  Approved!", "error");
        }
     )
  }

  suspendInstructorformdeclined(index){
    jQuery("#modal-info").modal("show");
    this.global_id =this.declinedLists[index].user_id;
    console.log('I ama Insinde suspend form declined')
    this.global_index=index;
    this.New_body="Do You Really Want to Suspend?"
    this.New_title="Suspend Instructor";
    this.New_status=7;   
  }
  suspendformdeclinedModel(){
    console.log('I am inside')
    jQuery("#modal-info").modal("hide");
    this.instructorService.declinedInstructor(this.global_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.declinedLists.splice(this.global_index,1)
              this.number_of_declined=this.number_of_declined-1;
              this.number_of_suspend=this.number_of_suspend+1;
              swal("Good job!", "Sucessfully Approved!", "success");
            }
        },(error)=>{
              swal("Opp...!", "Unable to  Approved!", "error");
        }
     )
  }

  declinedChange(index){
    console.log(index)
    let verified_data;
    let data=this.declinedform.value;
    for(let x in data.declined_List){
      if(this.declinedLists[index].user_id == data.declined_List[x].id){
           verified_data=data.declined_List[x];
           break;
        }
     }
     console.log(verified_data);
     this.instructorService.verifiedData(verified_data).subscribe(
      (response)=>{
        console.log(response)
      }
    )
  }
  AllSuspend(){
    console.log('i am All Suspend')
    this.all=false;
    this.is_approved=false;
    this.is_declined=false;
    this.is_suspend=true;
    this.instructorService.getSuspend().subscribe(
      (response)=>{
           console.log(response)
           this.suspendLists=response.suspend;
           for(let x in this.suspendLists){
            console.log('menus data',this.suspendLists[x].verified_data.email)
            this.suspendArray.push(this.fb.group({
              email: this.suspendLists[x].verified_data.email,
              phonenumber:this.suspendLists[x].verified_data.phonenumber,
              website: this.suspendLists[x].verified_data.website,
              socialmedial:this.suspendLists[x].verified_data.socialmedial,
              id:this.suspendLists[x].user_id,
              cover_image:this.suspendLists[x].cover_image,
          }));
        }
        this.suspendform= this.fb.group({
          suspend_List:this.fb.array(this.suspendArray)
         });
          $(function() {
            $('#suspendTable').DataTable();
            }); 
      })
  }
  Approvedformsuspend(index){
      jQuery("#modal-info").modal("show");
      this.global_id =this.suspendLists[index].user_id;
      this.global_index=index;
      this.New_body="Do You Really Want to Approved?"
      this.New_title="Approved Instructor";
      this.New_status=8;  
  
  }
  approvedformsuspendModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.approvedInstructor(this.global_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.suspendLists.splice(this.global_index,1);
              this.number_of_suspend=this.number_of_suspend-1;
              this.number_of_approved=this.number_of_approved+1;
              swal("Good job!", "Sucessfully Approved!", "success");
            }
        },(error)=>{
          swal("Opp...!", "Unable to Approved!", "error");
        }
     )
  }
  declinedInstructorformsuspend(index){
      jQuery("#modal-info").modal("show");
      this.global_id =this.suspendLists[index].user_id;
      this.global_index=index;
      this.New_body="Do You Really Want to Declined?"
      this.New_title="Declined Instructor";
      this.New_status=9; 
  }
  declinedformsuspendModel(){
    jQuery("#modal-info").modal("hide");
    this.instructorService.declinedInstructor(this.global_id).subscribe(
      (response)=>{
            console.log(response)
            if(response){
              this.suspendLists.splice(this.global_index,1);
              this.number_of_suspend=this.number_of_suspend-1;
              this.number_of_declined=this.number_of_declined+1;
              swal("Good job!", "Sucessfully Declined!", "success");
            }
        },(error)=>{
             swal("Opp...!", "Unable to  Declined!", "error");
        }
     )
  }
  suspendChange(index){
    let verified_data;
    let data=this.suspendform.value;
    for(let x in data.suspend_List){
      if(this.suspendLists[index].user_id == data.suspend_List[x].id){
           verified_data=data.suspend_List[x];
           break;
        }
     }
     console.log(verified_data);
     this.instructorService.verifiedData(verified_data).subscribe(
      (response)=>{
        console.log(response)
       
      }
    )
  }
}
