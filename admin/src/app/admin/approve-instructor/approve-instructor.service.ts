import { Injectable } from '@angular/core';
import {Http, Response ,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
@Injectable()
export class ApproveInstructorService {

  constructor(private http:Http) { }
  getInstructorApproved():Observable<any>{
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/get_instructor",options)
           .map(this.extractData)
           .catch(this.handleError);
    }
    private extractData(res: Response) {
           let response = res.json();
           return response || {};
    }
    private handleError(error: Response | any) {
          return Observable.throw(error);
  }
  verifiedData(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.post("http://classmonkserver.com/admin100/verified_data",data,options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  approvedInstructor(id){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/approved_instructor/"+id,options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  declinedInstructor(id){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/declined_instructor/"+id,options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  suspendInstructor(id){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/suspend_instructor/"+id,options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  getApproved(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/get_approved",options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  getDeclined(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/get_declined",options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  getSuspend(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/get_suspend",options)
           .map(this.extractData)
           .catch(this.handleError);
  }
  getCount(){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.get("http://classmonkserver.com/admin100/get_count",options)
           .map(this.extractData)
           .catch(this.handleError);
  }
}

