import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveInstructorComponent } from './approve-instructor.component';

describe('ApproveInstructorComponent', () => {
  let component: ApproveInstructorComponent;
  let fixture: ComponentFixture<ApproveInstructorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApproveInstructorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveInstructorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
