import { TestBed, inject } from '@angular/core/testing';

import { ApproveInstructorService } from './approve-instructor.service';

describe('ApproveInstructorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ApproveInstructorService]
    });
  });

  it('should be created', inject([ApproveInstructorService], (service: ApproveInstructorService) => {
    expect(service).toBeTruthy();
  }));
});
