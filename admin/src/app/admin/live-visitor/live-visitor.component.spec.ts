import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveVisitorComponent } from './live-visitor.component';

describe('LiveVisitorComponent', () => {
  let component: LiveVisitorComponent;
  let fixture: ComponentFixture<LiveVisitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveVisitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveVisitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
