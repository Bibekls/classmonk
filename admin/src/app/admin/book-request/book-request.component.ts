import { Component, OnInit } from '@angular/core';
declare var jQuery: any;

@Component({
  selector: 'app-book-request',
  templateUrl: './book-request.component.html',
  styleUrls: ['./book-request.component.css']
})
export class BookRequestComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  showModel(){
    jQuery("#modal-info").modal("show");
  }
}
