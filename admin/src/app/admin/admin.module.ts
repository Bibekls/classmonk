import { NgModule,ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { RouterModule, Routes} from '@angular/router';
import { HttpModule } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LiveVisitorComponent } from './live-visitor/live-visitor.component';
import { InquiryListComponent } from './inquiry-list/inquiry-list.component';
import { BookRequestComponent } from './book-request/book-request.component';
import { ApproveInstructorComponent } from './approve-instructor/approve-instructor.component';
import { FlotModule } from 'ng2modules-flot';
import * as $ from 'jquery';
import { ChartsModule } from 'ng2-charts';
import { NewClassesComponent } from './new-classes/new-classes.component';
import { TransactionComponent } from './transaction/transaction.component';
import { MyDatePickerModule } from 'mydatepicker';
import { InquiryListService } from './inquiry-list/inquiry-list.service';
import { NewClassesService } from './new-classes/new-classes.service';
import { ApproveInstructorService } from './approve-instructor/approve-instructor.service';
import { AgmCoreModule } from '@agm/core';
import { AdminGuardGuard } from '../admin-login/admin-guard.guard';

const adminRoutes: Routes = [
  { path: 'admin', component: AdminComponent,
  canActivate:[AdminGuardGuard],
  children: [
    { path: 'live-visitor', component: LiveVisitorComponent },     
    { path: 'inquiry-list', component:InquiryListComponent},
    { path: 'book-request', component: BookRequestComponent },
    { path: 'approve-instructor', component: ApproveInstructorComponent},
    { path: 'classes', component: NewClassesComponent},
    { path: 'transaction', component: TransactionComponent},      
    ]
  }      
];

@NgModule({
  imports: [
    CommonModule,
    HttpModule,
    FormsModule,
    RouterModule.forRoot(adminRoutes), 
    ReactiveFormsModule,
    FlotModule,
    ChartsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBTpvf3ap4iSjcsSC2QuFLfJWIUH3kOzJo',
      libraries: ["places"]
    }),
    MyDatePickerModule
  ],
  declarations: [LiveVisitorComponent,InquiryListComponent, BookRequestComponent, ApproveInstructorComponent, NewClassesComponent, TransactionComponent],
  bootstrap:[AdminComponent],
  providers:[InquiryListService,NewClassesService,ApproveInstructorService]
})
export class AdminModule { }
