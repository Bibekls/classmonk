import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-menus',
  templateUrl: './admin-menus.component.html',
  styleUrls: ['./admin-menus.component.css']
})
export class AdminMenusComponent implements OnInit {

  constructor(private router:Router) { }
public menulists=[];
public setclass;
  ngOnInit() {
    this.menulists = JSON.parse(localStorage.getItem('SelectMenuIten'));
    console.log('form Menuszlist', this.menulists)
  
  }
  // menulists = [
  //   {fa:'fa fa-dashboard', status:true, link:'/admin/live-visitor', name:' Live Visitor'},
  //   {fa:'fa fa-university', status:true, link:'/admin/inquiry-list', name:'Inquiry List'},
  //   // {fa:'fa fa-list', status:true, link:'/admin/book-request', name:'Book Request'},
  //   {fa:'fa fa-users', status:true, link:'/admin/approve-instructor', name:'Approve Instructor'},  
  //   {fa:'fa fa-users', status:true, link:'/admin/classes', name:'New Classes'}, 
  //   {fa:'fa fa-users', status:true, link:'/admin/transaction', name:'Transaction'}, 
  // ]
  loginOut(){
    this.router.navigate(['']);

  }
  addMenus(value){
    this.setclass=value
  }
}
