import { Component, OnInit } from '@angular/core';
declare var jQuery: any;
import { NewClassesService } from './new-classes.service';
declare var $;
import * as sweetalert from 'sweetalert';

@Component({
  selector: 'app-new-classes',
  templateUrl: './new-classes.component.html',
  styleUrls: ['./new-classes.component.css']
})
export class NewClassesComponent implements OnInit {
  lat: number = 51.678418;
  lng: number = 7.809007;
constructor(private classService:NewClassesService) { }
public newClass=[];
public is_approved=false;
public is_declined=false;
public is_new=true;
public approvedLists=[];
public declinedLists=[]
public pages=[]
public lastPage;
public ApprovedPage=[];
public approvedlastPage;
public declinedPage=[];
public declinedlastPage;
public global_index;
public global_id
public body;
public title;
public status;
  ngOnInit() {
    this.tempclass(); 
  }
  showModel(){
    jQuery("#modal-info").modal("show");
  }
  tempclass(){
    let Param={};
    Param['page']=1;
    this.classService.getNewClasses(Param).subscribe(
      (response)=>{
          console.log(response)
          this.newClass=response.newclass.data;
          Param['page']=response.newclass.current_page;
          this.lastPage=response.newclass.current_page;
          this.pages.push(Param)
          console.log(this.pages)
        }
      )
  }
approved(index){
  console.log(index)
  jQuery("#class-modal-info").modal("show");
  this.global_id =this.newClass[index].id;
  this.global_index=index;
  this.body="Do You Really Want to Approved?"
  this.title="Approved Class"
  this.status=1;
  }
  approvedformNewModel(){  
    jQuery("#class-modal-info").modal("hide");
    this.classService.postPruductIdForapproved(this.global_id).subscribe((response)=>{
      console.log(response)
         swal("Good job!", "Sucessfully Approved!", "success");
         this.newClass.splice(this.global_index,1);   
       },(error)=>{
         swal("Opp..!", "Unable to Approved!", "success");
       }
    )
  }
  declined(index){
    jQuery("#class-modal-info").modal("show");
    this.global_id =this.newClass[index].id;
    this.global_index=index;
    this.body="Do You Really Want to Declined?"
    this.title="Declined Class"
    this.status=2;
   } 
   declinedformNewModel(){
    jQuery("#class-modal-info").modal("hide");
    this.classService.postPruductIdFordeclined(this.global_id).subscribe((response)=>{
      console.log(response)
      swal("Good job!", "Sucessfully Declined!", "success");
        this.newClass.splice(this.global_index,1);
     },(error)=>{
      swal("Opp...!", "Unable to Declined!", "success");
     }
  )
   }
   newClassMethod(){
       this.is_new=true;
       this.is_approved=false;
       this.is_declined=false
   }
   approvedClassMethod(){
       this.is_new=false;
       this.is_approved=true;
       this.is_declined=false;
       let Param={};
       Param['page']=1;
       this.classService.approvedClass(Param).subscribe(
       (response)=>{
              console.log(response.approved);
              Param['page']=response.approved.current_page;
              this.approvedLists=response.approved.data;
              this.ApprovedPage.push(Param)
              this.approvedlastPage=response.approved.current_page;
            }
        )
   }
  


   declinedFromApprovedTab(index){
    jQuery("#class-modal-info").modal("show");
    this.global_id =this.approvedLists[index].id;
    this.global_index=index;
    this.body="Do You Really Want to Declined?"
    this.title="Declined Class"
    this.status=3;

   }
   suspendformApprovedModel(){
    jQuery("#class-modal-info").modal("hide");
    this.classService.postPruductIdFordeclined(this.global_id).subscribe(
      (response)=>{
          console.log(response)
            swal("Good job!", "Sucessfully Declined!", "success");
            this.approvedLists.splice(this.global_index,1);
         },(error)=>{
          swal("Opp...!", "Unable to Decliend!", "success");

        }
      )
   }
   declinedClassMethod(){
       this.is_new=false;
       this.is_declined=true;
       this.is_approved=false
       let Param={};
       Param['page']=1
       this.classService.declinedClass(Param).subscribe(
        (response)=>{
                  console.log(response.declined.data);
                  this.declinedLists=response.declined.data;
                  Param['page']=response.declined.current_page
                  this.declinedPage.push(Param);
                  this.declinedlastPage=response.declined.current_page;
         }
      )
   }
   approvedFormDeclinedTab(index){
    jQuery("#class-modal-info").modal("show");
    this.global_id =this.approvedLists[index].id;
    this.global_index=index;
    this.body="Do You Really Want to Approved?"
    this.title="Approved Class"
    this.status=4;
   }
   approvedformDeclinedModel(){
    jQuery("#class-modal-info").modal("hide");
    this.classService.postPruductIdForapproved(this.global_id).subscribe(
      (response)=>{
         console.log(response)
            swal("Good job!", "Sucessfully Approved!", "success");
            this.declinedLists.splice(this.global_index,1);   
          },(error)=>{
            swal("Opp...!", "Unable to Approved!", "success");

          }
       )
   }
Previous(){
   console.log('I ma Inside')
   if(this.lastPage !=1){
    console.log('I ma Not Ready')
    let Param={};
    this.lastPage=parseInt(this.lastPage)-1;
    Param['page']=this.lastPage;
    console.log(Param)
    this.classService.getNewClasses(Param).subscribe(
      (response)=>{
          console.log(response)
          this.newClass=response.newclass.data;
          Param['page']=response.newclass.current_page;
          this.lastPage=response.newclass.current_page;
          if(this.pages.length > 0){
            console.log('I am response')
             this.pages.splice(-1);
           }
          console.log(this.pages)
        }
     )
   }
 }
Next(){
  console.log('I ma Inside')
  let Param={}; 
  this.lastPage=parseInt(this.lastPage)+1;
  Param['page']=this.lastPage;
  console.log(Param)
  this.classService.getNewClasses(Param).subscribe(
    (response)=>{
        console.log(response)
        this.newClass=response.newclass.data;
        Param['page']=response.newclass.current_page;
        this.lastPage=response.newclass.current_page;
        if(this.pages.length < 4){
          this.pages.push(Param)
        }
        console.log(this.pages)
      }
   )
 }
 middlePages(i){
  let Param={}; 
  console.log(this.pages[i])
  Param['page']=this.pages[i].page;
  this.classService.getNewClasses(Param).subscribe(
    (response)=>{
        console.log(response)
        this.newClass=response.newclass.data;
        
       }
    )
  }
PreviousApproved(){
  let Param={}
  if(this.approvedlastPage !=1){
    this.approvedlastPage=parseInt(this.approvedlastPage)-1
    Param['page']=this.approvedlastPage;
    this.classService.approvedClass(Param).subscribe(
      (response)=>{
         this.approvedLists=response.approved.data;
           Param['page']=response.approved.current_page;
             this.approvedlastPage=response.approved.current_page;
               if(this.ApprovedPage.length > 0){
                   console.log('I am response')
                   this.ApprovedPage.splice(-1);
              }
           }
        )
    }
 } 
NextApproved(){
  let Param={};
  this.approvedlastPage=parseInt(this.approvedlastPage)+1
  Param['page']=this.approvedlastPage;
  this.classService.approvedClass(Param).subscribe(
    (response)=>{
           console.log(response.approved);
           Param['page']=response.approved.current_page;
           this.approvedLists=response.approved.data;
           if(this.ApprovedPage.length < 4){
             this.ApprovedPage.push(Param)
           }
           this.approvedlastPage=response.approved.current_page;
         }
     )
 } 
middlePagesApproved(i){
  let Param={}; 
  Param['page']=this.ApprovedPage[i].page;
  this.classService.approvedClass(Param).subscribe(
    (response)=>{
        console.log(response)
        this.approvedLists=response.approved.data;
       }
    )
  }
  Previousdeclined(){
    if(this.declinedlastPage !=1){
    let Param={}
    this.declinedlastPage=parseInt(this.declinedlastPage)-1
    Param['page']=this.declinedlastPage;
    this.classService.declinedClass(Param).subscribe(
     (response)=>{
             console.log(response.declined.data);
             this.declinedLists=response.declined.data;
             if(this.declinedPage.length > 0){
               this.declinedPage.splice(-1);
             }
          }
       )
    }
  }
  middlePagesDeclined(i){
    let Param={} 
    Param['page']=this.declinedPage[i].page;
    this.classService.declinedClass(Param).subscribe(
     (response)=>{
             console.log(response.declined.data);
             this.declinedLists=response.declined.data;
          } 
       )
  }
  NextDeclined(){
   let Param={}
   this.declinedlastPage=parseInt(this.declinedlastPage)+1
   Param['page']=this.declinedlastPage;
   this.classService.declinedClass(Param).subscribe(
    (response)=>{
            console.log(response.declined.data);
            this.declinedLists=response.declined.data;
            Param['page']=response.declined.current_page;
            if(this.declinedPage.length < 4){
              this.declinedPage.push(Param);
             }
            this.declinedlastPage=response.declined.current_page;
        }
      )
    }
}
