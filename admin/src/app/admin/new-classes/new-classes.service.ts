import { Injectable } from '@angular/core';
import {Http, Response ,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
@Injectable()
export class NewClassesService {

  constructor(private http:Http) { }
 
  getNewClasses(data):Observable<any>{
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers, withCredentials: true});
    return this.http.post("http://classmonkserver.com/admin100/getnewclasses",data,options)
           .map(this.extractData)
           .catch(this.handleError);
    }
    private extractData(res: Response) {
           let response = res.json();
           return response || {};
    }
    private handleError(error: Response | any) {
          return Observable.throw(error);
}
postPruductIdForapproved(id){
  let headers = new Headers({'Content-Type': 'application/json'});
  let options = new RequestOptions({headers: headers,withCredentials: true});
   return this.http.get("http://classmonkserver.com/admin100/productidForapproved/"+id,options)
  .map(this.extractData)
  .catch(this.handleError); 
  }
  postPruductIdFordeclined(id){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.get("http://classmonkserver.com/admin100/productidFordecliend/"+id,options)
    .map(this.extractData)
    .catch(this.handleError); 
  } 
  approvedClass(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.post("http://classmonkserver.com/admin100/approved_class",data,options)
    .map(this.extractData)
    .catch(this.handleError); 
  }
  declinedClass(data){
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers,withCredentials: true});
     return this.http.post("http://classmonkserver.com/admin100/declined_class",data,options)
    .map(this.extractData)
    .catch(this.handleError); 
  }
}
