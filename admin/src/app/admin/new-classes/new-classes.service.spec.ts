import { TestBed, inject } from '@angular/core/testing';

import { NewClassesService } from './new-classes.service';

describe('NewClassesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NewClassesService]
    });
  });

  it('should be created', inject([NewClassesService], (service: NewClassesService) => {
    expect(service).toBeTruthy();
  }));
});
