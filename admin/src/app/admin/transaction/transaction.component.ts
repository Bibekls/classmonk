import { Component, OnInit } from '@angular/core';
import {IMyDpOptions} from 'mydatepicker';
import { FormGroup, FormControl, Validators,FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {
  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd.mm.yyyy',
   };
   public myForm: FormGroup;
  public pending=true;
  public completed=false;
  constructor(private formBuilder: FormBuilder) { 
    this.myForm = this.formBuilder.group({
      // Empty string or null means no initial value. Can be also specific date for
      // example: {date: {year: 2018, month: 10, day: 9}} which sets this date to initial
      // value.

      FromDate: [null, Validators.required],
      ToDate: [null, Validators.required]
      // other controls are here...
  });
  }

  ngOnInit() {

  }
  setDate(): void {
    // Set today date using the patchValue function
    let date = new Date();
    this.myForm.patchValue({FromDate: {
    date: {
        year: date.getFullYear(),
        month: date.getMonth() + 1,
        day: date.getDate()}
    }});
    this.myForm.patchValue({ToDate: {
      date: {
          year: date.getFullYear(),
          month: date.getMonth() + 1,
          day: date.getDate()}
      }});
}

clearDate(): void {
    // Clear the date using the patchValue function
    this.myForm.patchValue({FromDate: null});
    this.myForm.patchValue({ToDate: null});
}
PendingTransaction(){
  this.pending=true;
  this.completed=false;
}
CompletedTransaction(){
  this.pending=false;
  this.completed=true;
 }
}
