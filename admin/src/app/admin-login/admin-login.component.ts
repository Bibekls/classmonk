import { Component, OnInit } from '@angular/core';
import { FormBuilder,FormArray, FormGroup, Validators, FormControl} from '@angular/forms';
import { AdminLoginService } from './admin-login.service';
import { GuardserviceService } from './guardservice.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})
export class AdminLoginComponent implements OnInit {
  public loginForm:FormGroup;
  public Info=false;
  public info;
  public sign="Sing In"
  public Sing=false;
  constructor( private loginService:AdminLoginService,private router:Router) { 
    this.loginForm = new FormGroup({
      email:new FormControl(['']),
      password:new FormControl([''])
    })
  }
  
  ngOnInit() {
  }
  loginFormSubmit(){
   this.Sing=true;
   this.sign="Singing..."
   console.log(this.loginForm.value)
   if(this.loginForm.valid){
    let Data=this.loginForm.value
    this.loginService.UserInfo(Data).subscribe(
       (response)=>{
        console.log(response)
        GuardserviceService.setLoggedInStatus(true)
        console.log(response.user)
        console.log(response.user.menus)
        localStorage.setItem('SelectMenuIten', JSON.stringify(response.user.menus));
        this.router.navigate(['admin/live-visitor']);
        localStorage.setItem("user",JSON.stringify(response.user))
        this.Sing=false;
        this.sign="Sing In"
        },(error)=>{
          if (error.status === 500) {
            this.Sing=false;
            setTimeout(function () {
              this.Info = false;
            }.bind(this), 3000);  
            this.sign="Sing In"
            this.Info=true;
          this.info='You are not authorized user';
        } else {
            this.Info=true;
            this.Sing=false;
            setTimeout(function () {
              this.false = true;
            }.bind(this), 3000);  
            this.sign="Sing In"
            this.info='Sorry the credential missmatch';
          }
       }
      )
    }
  }
}
