import { Injectable } from '@angular/core';
import {Http, Response ,Headers, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
@Injectable()
export class AdminLoginService {
 public info;
 public Info=false;
  constructor(private http:Http) { }
  UserInfo(data){
    let options = new RequestOptions({headers: headers, withCredentials: true});
    var headers = new Headers({
      "Content-Type": "application/json",
      "Accept": "application/json"
   });
    return this.http.post("http://classmonkserver.com/admin100/userinfo",data,options)
           .map(this.extractData)
           .catch(this.handleError);
    }
    private extractData(res: Response) {
           let response = res.json();
           return response || {};
    }
    private handleError(error: Response | any) {
          return Observable.throw(error);
  }
}
