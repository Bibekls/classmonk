import { Injectable } from '@angular/core';
import { CanActivate, Router,ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { GuardserviceService } from './guardservice.service';

@Injectable()
export class AdminGuardGuard implements CanActivate {
 constructor(private user:GuardserviceService,private router:Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      if(GuardserviceService.getLoggedInStatus()){
        return true;
     } else{
       this.router.navigate(['/']);
    }
  }
}
