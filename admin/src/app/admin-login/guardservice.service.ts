import { Injectable } from '@angular/core';

@Injectable()
export class GuardserviceService {

  constructor() { }
  private static loggedIn:boolean = false;
  public static setLoggedInStatus(status){
    GuardserviceService.loggedIn = status
  }
  public static getLoggedInStatus(){
    return GuardserviceService.loggedIn
    }
}
