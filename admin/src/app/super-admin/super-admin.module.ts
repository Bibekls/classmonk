import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { SuperAdminComponent } from './super-admin.component';
import { HttpModule } from "@angular/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { ManageCategoryComponent } from './manage-category/manage-category.component';
import { ManageAdminUserComponent } from './manage-admin-user/manage-admin-user.component';
import { AddCityComponent } from './add-city/add-city.component';
import { AgmCoreModule } from '@agm/core';
// import * as $ from 'jquery';
// import { FlotModule } from 'ng2modules-flot';

const super_adminRoutes: Routes = [
  { path: 'super-admin', component: SuperAdminComponent,
  children: [
    { path: 'dashboard', component: AdminDashboardComponent },     
    { path: 'city', component:AddCityComponent},
    { path: 'category', component: ManageCategoryComponent },
    { path: 'user', component: ManageAdminUserComponent},      
    ]
  }      
];
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forRoot(super_adminRoutes),
    HttpModule,
    // FlotModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBTpvf3ap4iSjcsSC2QuFLfJWIUH3kOzJo'
    })
  ],
  declarations: [AdminDashboardComponent,AddCityComponent , ManageCategoryComponent, ManageAdminUserComponent],
  bootstrap:[SuperAdminComponent],
})
export class SuperAdminModule { }
