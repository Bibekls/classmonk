import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminMenusComponent } from './super-admin-menus.component';

describe('SuperAdminMenusComponent', () => {
  let component: SuperAdminMenusComponent;
  let fixture: ComponentFixture<SuperAdminMenusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminMenusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminMenusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
