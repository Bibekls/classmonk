import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-super-admin-menus',
  templateUrl: './super-admin-menus.component.html',
  styleUrls: ['./super-admin-menus.component.css']
})
export class SuperAdminMenusComponent implements OnInit {

  constructor() { }

  ngOnInit() {
   
  }
  menulists = [
    {fa:'fa fa-dashboard', status:true, link:'/super-admin/dashboard', name:'Dashboard'},
    {fa:'fa fa-university', status:true, link:'/super-admin/city', name:'Add City'},
    {fa:'fa fa-list', status:true, link:'/super-admin/category', name:'Manage Category'},
    {fa:'fa fa-users', status:true, link:'/super-admin/user', name:'Manage Admin User'},       
  ]

}
