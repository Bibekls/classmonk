import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { AppComponent } from './app.component';
import { SuperAdminComponent } from './super-admin/super-admin.component';
import { HttpModule } from "@angular/http";
import { SuperAdminModule} from './super-admin/super-admin.module';
import { SuperAdminMenusComponent } from './super-admin/super-admin-menus/super-admin-menus.component';
import { AdminComponent } from './admin/admin.component';
import { AdminModule } from './admin/admin.module';
import { AdminMenusComponent } from './admin/admin-menus/admin-menus.component';
import { AdminLoginComponent } from './admin-login/admin-login.component';
// import { FlotModule } from 'ng2modules-flot';
// import * as $ from 'jquery';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLoginService } from './admin-login/admin-login.service';
import { GuardserviceService } from './admin-login/guardservice.service';
import { AdminGuardGuard } from './admin-login/admin-guard.guard';

const appRoutes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', component: AdminLoginComponent },
  { path: 'login', component: AdminLoginComponent },
  { path: 'admin', component: AdminComponent },
  { path: 'super-admin', component: SuperAdminComponent },
];
@NgModule({
  declarations: [
    AppComponent,
    SuperAdminComponent,
    SuperAdminMenusComponent,
    AdminMenusComponent,
    AdminComponent,
    AdminLoginComponent,
    
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes), 
    SuperAdminModule,
    AdminModule,
    FormsModule,
    ReactiveFormsModule
    // FlotModule
  ],
  providers: [AdminLoginService,AdminGuardGuard,GuardserviceService],
  bootstrap: [AppComponent],

})
export class AppModule { }
